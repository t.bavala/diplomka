package com.diplomka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class DiplomkaApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(DiplomkaApplication.class, args);
	}

}

