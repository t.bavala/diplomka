package com.diplomka.configuration;



public final class Constants {

	// Regex for acceptable logins
	public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

	public static final String UPLOADS_PATH = "diplomka-client/src/uploads/";

	private Constants() {
	}
}

