package com.diplomka.conference.repository;

import com.diplomka.conference.repository.entity.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SectionRepository extends JpaRepository<Section,Long> {
	List<Section> findAllByConference_IdAndDateOrderByTimeFromAsc (Long id, Date date);

	List<Section> findAllByConference_Id(Long id);
}
