package com.diplomka.conference.repository;

import com.diplomka.conference.repository.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article,Long> {
	List<Article> findAllBySection_Id(Long id);

	List<Article> findAllBySection_IdAndUser_Id(Long id, Long userid);
}
