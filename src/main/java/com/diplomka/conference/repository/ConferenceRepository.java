package com.diplomka.conference.repository;

import com.diplomka.conference.repository.entity.Conference;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ConferenceRepository extends JpaRepository<Conference,Long> {
	Optional<Conference> findById (Long id);

	List<Conference> findAllByDateFromGreaterThanEqualOrderByDateFromAsc( Date date);

	List<Conference> findAllByDateFromLessThanOrderByDateFromDesc( Date date);

	List<Conference> findAllByOrderByDateFromDesc();
}
