package com.diplomka.conference.repository.entity;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class SqlTimeDeserializer extends JsonDeserializer<java.sql.Time> {

	@Override
	public Time deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
		ObjectCodec oc = jsonParser.getCodec();
		JsonNode node = oc.readTree(jsonParser);
		String time = node.textValue();
		DateFormat sdf = new SimpleDateFormat("HH:mm");
		try {
			Date date = sdf.parse(time);
			return new Time(date.getTime());// convert java.sql.Time from string
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
