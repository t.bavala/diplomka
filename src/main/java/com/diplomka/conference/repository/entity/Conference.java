package com.diplomka.conference.repository.entity;

import com.diplomka.auth.repository.entity.Address;
import com.diplomka.auth.repository.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "conference")
public class Conference {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="title", unique = true, nullable = false)
	private String title;

	@Column(name="date_from")
	private Date dateFrom;

	@Column(name="date_to")
	private Date dateTo;

	@Column(name="description", columnDefinition="TEXT")
	private String description;

	@JsonIgnore
	@ManyToMany
	@JoinTable(
	name = "conference_user",
	joinColumns = {@JoinColumn(name = "conference_id", referencedColumnName = "id")},
	inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")})
	private Set<User> users = new HashSet<>();

	@OneToMany(mappedBy="conference", cascade = CascadeType.ALL)
	@JsonIgnore
	private Set<Section> sections = new HashSet<>();

	@ManyToOne(targetEntity=Address.class, cascade = CascadeType.ALL)
	@JsonIgnore
	private Address address;

	@JsonIgnore
	@OneToMany(targetEntity=Post.class, mappedBy="conference")
	private Set<Post> posts = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<Section> getSections() {
		return sections;
	}

	public void setSections(Set<Section> sections) {
		this.sections = sections;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}
}
