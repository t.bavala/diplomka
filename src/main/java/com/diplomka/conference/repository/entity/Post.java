
package com.diplomka.conference.repository.entity;

import com.diplomka.auth.repository.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="post")
public class Post {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="post_text", columnDefinition="TEXT")
	private String postText;

	@Column(name="created_at")
	private Date createdAt;

	@ManyToOne(targetEntity= User.class, cascade = CascadeType.ALL)
	private User user;

	@ManyToOne(targetEntity= Conference.class, cascade = CascadeType.ALL)
	private Conference conference;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPostText() {
		return postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Conference getConference() {
		return conference;
	}

	public void setConference(Conference conference) {
		this.conference = conference;
	}
}

