package com.diplomka.conference.repository.entity;

import com.diplomka.auth.dto.UserDTO;
import com.diplomka.auth.repository.entity.Address;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class CreateConference {

	private Long id;

	private String title;

	private Date dateFrom;

	private Date dateTo;

	private String description;

	private Set<UserDTO> users = new HashSet<>();

	private Address address;

	public CreateConference(Conference conference, Set<UserDTO> users) {
		this.id = conference.getId();
		this.title = conference.getTitle();
		this.dateFrom = conference.getDateFrom();
		this.dateTo = conference.getDateTo();
		this.description = conference.getDescription();
		this.users = users;
		this.address = conference.getAddress();
	}

	public CreateConference(){

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<UserDTO> getUsers() {
		return users;
	}

	public void setUsers(Set<UserDTO> users) {
		this.users = users;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
}
