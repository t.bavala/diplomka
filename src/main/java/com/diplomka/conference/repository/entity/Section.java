package com.diplomka.conference.repository.entity;

import com.diplomka.auth.repository.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.context.annotation.Configuration;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="section")
public class Section {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private Conference conference;

	@Column(name="room")
	private String room;

	@Column(name="title")
	private String title;

	@Column(name="name")
	private String name;

	@JsonDeserialize(using = SqlTimeDeserializer.class)
	@Column(name="time_from")
	private Time timeFrom;

	@JsonDeserialize(using = SqlTimeDeserializer.class)
	@Column(name="time_to")
	private Time timeTo;

	@Column(name="date")
	private Date date;

	@Column(name="description")
	private String description;

	@Column(name="chair")
	private String chair;

	@Column(name="assistants")
	private String assistants;

	@ManyToOne(targetEntity=User.class, cascade = CascadeType.ALL)
	private User user;

	@NotNull
	@Column(name="isForAll", nullable = false)
	private boolean isForAll = false;

	@OneToMany(mappedBy="section", cascade = CascadeType.ALL)
	@JsonIgnore
	private Set<Article> articles = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Conference getConference() {
		return conference;
	}

	public void setConference(Conference conference) {
		this.conference = conference;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Time getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(Time timeFrom) {
		this.timeFrom = timeFrom;
	}

	public Time getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(Time timeTo) {
		this.timeTo = timeTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getChair() {
		return chair;
	}

	public void setChair(String chair) {
		this.chair = chair;
	}

	public String getAssistants() {
		return assistants;
	}

	public void setAssistants(String assistants) {
		this.assistants = assistants;
	}

	public boolean isForAll() {
		return isForAll;
	}

	public void setForAll(boolean forAll) {
		isForAll = forAll;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public Set<Article> getArticles() {
		return articles;
	}

	public void setArticles(Set<Article> articles) {
		this.articles = articles;
	}
}
