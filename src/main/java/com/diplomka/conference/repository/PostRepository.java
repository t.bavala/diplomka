package com.diplomka.conference.repository;
import com.diplomka.conference.repository.entity.Article;
import com.diplomka.conference.repository.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post,Long> {
	List<Post> findAllByConference_Id(Long id);
}
