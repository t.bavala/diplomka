package com.diplomka.conference.service;
import com.diplomka.conference.repository.SectionRepository;
import com.diplomka.conference.repository.entity.Section;
import com.mysql.cj.xdevapi.JsonArray;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static com.diplomka.configuration.Constants.UPLOADS_PATH;

@Service
@Transactional
public class SectionService {

	private SectionRepository sectionRepository;

	public SectionService(SectionRepository sectionRepository) {
		this.sectionRepository = sectionRepository;
	}

	public Section createSection(Section section) {
		Section section1 = new Section();
		section1.setId(section.getId());
		section1.setConference(section.getConference());
		section1.setRoom(section.getRoom());
		section1.setTitle(section.getTitle());
		section1.setName(section.getName());
		section1.setTimeFrom(section.getTimeFrom());
		section1.setTimeTo(section.getTimeTo());
		section1.setDate(section.getDate());
		section1.setDescription(section.getDescription());
		section1.setChair(section.getChair());
		section1.setAssistants(section.getAssistants());
		section1.setUser(section.getUser());
		section1.setForAll(section.isForAll());
		sectionRepository.save(section1);
		createSectionFolder(section1);
		return section1;
	}

	private void createSectionFolder(Section section1) {
		File file = new File(UPLOADS_PATH+"conferences/"+section1.getConference().getId() +"/" + section1.getId());
		if (file.mkdir()) {
			System.out.println("Directory is created!");
		} else {
			System.out.println("Failed to create directory!");
		}
	}

	public List<Section> getSectionsOfConference(Long id, String date) {
		SimpleDateFormat sdf3 = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss 'GMT'K", Locale.ENGLISH);

		Date d1 = null;
		try{
			d1 = sdf3.parse(date);

		}catch (Exception e){ e.printStackTrace(); }
		List<Section> sections = sectionRepository.findAllByConference_IdAndDateOrderByTimeFromAsc(id, d1);
		return sections;
	}

	public List<Section> getSectionsOfConference(Long id) {
		List<Section> sections = sectionRepository.findAllByConference_Id(id);
		return sections;
	}

	public void deleteSection(Long id) {
		Optional.of(sectionRepository.findById(id)).ifPresent(section -> {
			sectionRepository.delete(section.get());
			try {
				deleteSectionFolder(section);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	private void deleteSectionFolder(Optional<Section> section) throws IOException {
		FileUtils.deleteDirectory(new File(UPLOADS_PATH+"conferences/"+section.get().getConference().getId()+"/"+section.get().getId()));
	}

	public Section updateSection(Section section) {
		return Optional.of(sectionRepository
				.findById(section.getId()))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(section1 -> {
					section1.setRoom(section.getRoom());
					section1.setTitle(section.getTitle());
					section1.setName(section.getName());
					section1.setTimeFrom(section.getTimeFrom());
					section1.setTimeTo(section.getTimeTo());
					section1.setDate(section.getDate());
					section1.setDescription(section.getDescription());
					section1.setChair(section.getChair());
					section1.setAssistants(section.getAssistants());
					section1.setUser(section.getUser());
					section1.setForAll(section.isForAll());
					return section1;
				}).get();
	}

	public void uploadSectionFiles(MultipartFile[] files, Long id) throws IOException {
		Section section = sectionRepository.findById(id).get();
		for (MultipartFile file : files) {
			File file1 = convert(file, section);
		}
	}

	public File convert(MultipartFile file, Section section) throws IOException {
		File convFile = new File(UPLOADS_PATH +"conferences/"+section.getConference().getId()+"/"+section.getId()+"/"+file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}


}




