package com.diplomka.conference.service;

import com.diplomka.auth.dto.UserDTO;
import com.diplomka.auth.repository.AddressRepository;
import com.diplomka.auth.repository.UserRepository;
import com.diplomka.auth.repository.entity.Address;
import com.diplomka.auth.repository.entity.User;
import com.diplomka.conference.repository.ConferenceRepository;
import com.diplomka.conference.repository.entity.Conference;
import com.diplomka.conference.repository.entity.CreateConference;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

import static com.diplomka.configuration.Constants.UPLOADS_PATH;

@Service
@Transactional
public class ConferenceService {

	private ConferenceRepository conferenceRepository;
	private UserRepository userRepository;
	private AddressRepository addressRepository;

	public ConferenceService(ConferenceRepository conferenceRepository, UserRepository userRepository, AddressRepository addressRepository) {
		this.conferenceRepository = conferenceRepository;
		this.userRepository = userRepository;
		this.addressRepository = addressRepository;

	}

	public List<Conference> getAllConferences() {
		return conferenceRepository.findAllByOrderByDateFromDesc();
	}

	public Conference createConference(CreateConference conference) {
		Conference conference1 = new Conference();
		conference1.setId(conference.getId());
		conference1.setTitle(conference.getTitle());
		conference1.setDateFrom(conference.getDateFrom());
		conference1.setDateTo(conference.getDateTo());
		conference1.setDescription(conference.getDescription());
		conference1.setAddress(conference.getAddress());
		if (conference.getUsers() != null) {
			Set<User> users = new HashSet<>();
			for (UserDTO user : conference.getUsers()) {
				User user1 = userRepository.getOne(user.getId());
				if (user1 != null) {
					users.add(user1);
				}
			}
			conference1.setUsers(users);
		}
		conferenceRepository.save(conference1);

		createConferenceFolder(conference1);
		return conference1;
	}

	private void createConferenceFolder(Conference conference1) {
		File file = new File(UPLOADS_PATH+"conferences/"+conference1.getId());
		if (file.mkdir()) {
			System.out.println("Directory is created!");
		} else {
			System.out.println("Failed to create directory!");
		}
	}

	public Conference updateConference(CreateConference conference) {
		return Optional.of(conferenceRepository
				.findById(conference.getId()))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(conference1 -> {
					conference1.setTitle(conference.getTitle());
					conference1.setDateFrom(conference.getDateFrom());
					conference1.setDateTo(conference.getDateTo());
					conference1.setDescription(conference.getDescription());
					this.saveAddress(conference.getAddress());

					Set<User> users = conference1.getUsers();
					users.clear();
					for (UserDTO user : conference.getUsers()) {
						User user1 = userRepository.findByUsername(user.getUsername());
						if (user1 != null) {
							users.add(user1);
						}
					}
					conference1.setUsers(users);
					return conference1;
				}).get();
	}


	public CreateConference getConference(Long id) {
		Optional<Conference> conference = conferenceRepository.findById(id);
		Conference conference1 = conference.get();
		Set<UserDTO> users = new HashSet<>();
		for (User user : conference1.getUsers()) {
			UserDTO userDTO = new UserDTO(user);
			users.add(userDTO);
		}
		return new CreateConference(conference1, users);
	}

	public void deleteConference(Long id) {
		Optional.of(conferenceRepository.findById(id)).ifPresent(conference -> conferenceRepository.delete(conference.get()));
		try {
			deleteConferenceFolder(id);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void deleteConferenceFolder(Long id) throws IOException {
		FileUtils.deleteDirectory(new File(UPLOADS_PATH+"conferences/"+id));
	}

	public CreateConference getClosestConference() {
		LocalDate date = LocalDate.now();
		List<Conference> list = this.conferenceRepository.findAllByDateFromGreaterThanEqualOrderByDateFromAsc(java.sql.Date.valueOf(date));
		if (list.size() > 0) {
			return getConference(list.get(0).getId());
		}else {
			list = this.conferenceRepository.findAllByDateFromLessThanOrderByDateFromDesc(java.sql.Date.valueOf(date));
			if (list.size() > 0){
				return getConference(list.get(0).getId());
			}else {
				return null;
			}

		}
	}

	private void saveAddress(Address address) {
		Optional.of(addressRepository
				.findById(address.getId()))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(address1 -> {
					address1.setStreet(address.getStreet());
					address1.setCity(address.getCity());
					address1.setCountry(address.getCountry());
					address1.setZipCode(address.getZipCode());
					return address1;
				});
	}
}



