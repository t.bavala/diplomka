package com.diplomka.conference.service;

import com.diplomka.auth.dto.UserDTO;
import com.diplomka.auth.repository.UserRepository;
import com.diplomka.auth.repository.entity.User;
import com.diplomka.auth.security.SecurityUtils;
import com.diplomka.conference.repository.ArticleRepository;
import com.diplomka.conference.repository.PostRepository;
import com.diplomka.conference.repository.entity.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.diplomka.configuration.Constants.UPLOADS_PATH;

@Service
@Transactional
public class PostService {

	private ArticleRepository articleRepository;
	private ConferenceService conferenceService;
	private SectionService sectionService;
	private PostRepository postRepository;
	private UserRepository userRepository;

	public PostService(ConferenceService conferenceService,
						  PostRepository postRepository,
					   UserRepository userRepository) {
		this.conferenceService = conferenceService;
		this.postRepository = postRepository;
		this.userRepository = userRepository;
	}


	public List<Post> getNewsOfConference() {
		CreateConference conference = conferenceService.getClosestConference();
		return postRepository.findAllByConference_Id(conference.getId());
	}

	public void updateNews(Post post) {
		Optional.of(postRepository
				.findById(post.getId()))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(post1 -> {
					post1.setPostText(post.getPostText());
					post1.setUser(userRepository.findByUsername(post.getUser().getUsername()));
					post1.setConference(post.getConference());
					post1.setCreatedAt(new Date());
					return post;
				}).get();
	}

	public Post createNews(Post post) {
		Post post1 = new Post();
		post1.setPostText(post.getPostText());
		post1.setUser(userRepository.findByUsername(post.getUser().getUsername()));
		post1.setConference(post.getConference());
		post1.setCreatedAt(new Date());
		postRepository.save(post1);
		return post1;
	}
}
