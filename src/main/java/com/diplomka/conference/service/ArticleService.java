package com.diplomka.conference.service;

import com.diplomka.auth.dto.UserDTO;
import com.diplomka.auth.repository.UserRepository;
import com.diplomka.auth.repository.entity.User;
import com.diplomka.auth.security.SecurityUtils;
import com.diplomka.conference.repository.ArticleRepository;
import com.diplomka.conference.repository.entity.Article;
import com.diplomka.conference.repository.entity.Conference;
import com.diplomka.conference.repository.entity.CreateConference;
import com.diplomka.conference.repository.entity.Section;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.diplomka.configuration.Constants.UPLOADS_PATH;

@Service
@Transactional
public class ArticleService {

	private ArticleRepository articleRepository;
	private ConferenceService conferenceService;
	private SectionService sectionService;
	private UserRepository userRepository;

	public ArticleService(ArticleRepository articleRepository,
						  ConferenceService conferenceService,
						  SectionService sectionService,
						  UserRepository userRepository) {
		this.articleRepository = articleRepository;
		this.conferenceService = conferenceService;
		this.sectionService = sectionService;
		this.userRepository = userRepository;
	}

	public List<Article> getArticlesOfSection(Long id) {
		return this.articleRepository.findAllBySection_Id(id);
	}

	public List<Article> getArticlesOfConference() {
		CreateConference conference = conferenceService.getClosestConference();
		List<Section> sections = sectionService.getSectionsOfConference(conference.getId());
		List<Article> allArticles = new ArrayList<>();
		for (Section section : sections) {
			List<Article> sectionArticle = this.articleRepository.findAllBySection_Id(section.getId());
			allArticles.addAll(sectionArticle);
		}
		return allArticles;
	}

	public void updateArticles(Article[] articles) {
		for (Article article : articles) {
			if (article.getId() == null) {
				createArticle(article);
			} else {
				updateArticle(article);
			}

		}
	}

	private void updateArticle(Article article) {
		Optional.of(articleRepository
			.findById(article.getId()))
			.filter(Optional::isPresent)
			.map(Optional::get)
			.map(article1 -> {
				if (!article1.getFilename().equals(article.getFilename()))
					this.deleteFile(article1);
				article1.setFilename(article.getFilename());
				article1.setAuthors(article.getAuthors());
				article1.setTitle(article.getTitle());
				article1.setAbstractText(article.getAbstractText());
				if (article.getUser() != null){
					article1.setUser(userRepository.findByUsername(article.getUser().getUsername()));
				} else {
					article1.setUser(article.getUser());
				}
				return article;
			}).get();
	}

	private void createArticle(Article article) {
		Article article1 = new Article();
		article1.setFilename(article.getFilename());
		article1.setAuthors(article.getAuthors());
		article1.setSection(article.getSection());
		article1.setTitle(article.getTitle());
		article1.setAbstractText(article.getAbstractText());
		article1.setUser(article.getUser());
		articleRepository.save(article1);
	}

	public void deleteArticle(Long id) {
		Optional.of(articleRepository.findById(id)).ifPresent(conference -> articleRepository.delete(conference.get()));
	}

	public void deleteFile(Article article){
		File file = new File(UPLOADS_PATH +"conferences/"+article.getSection().getConference().getId()+"/"+ article.getSection().getId()+"/"+article.getFilename());
		file.delete();
 	}

	public List<Article> getUserArticlesOfConference() {
		CreateConference conference = conferenceService.getClosestConference();
		List<Section> sections = sectionService.getSectionsOfConference(conference.getId());
		List<Article> allArticles = new ArrayList<>();
		String username = SecurityUtils.getCurrentUserLogin().get();
		User user = userRepository.findByUsername(username);
		for (Section section : sections) {
			List<Article> sectionArticle = this.articleRepository.findAllBySection_IdAndUser_Id(section.getId(), user.getId());
			allArticles.addAll(sectionArticle);
		}
		return allArticles;
	}
}
