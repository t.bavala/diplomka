package com.diplomka.conference.controller;

import com.diplomka.auth.security.RolesConstants;
import com.diplomka.conference.repository.entity.Conference;
import com.diplomka.conference.repository.entity.CreateConference;
import com.diplomka.conference.service.ConferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ConferenceController {
	private ConferenceService conferenceService;

	@Autowired
	public void setClientService(ConferenceService conferenceService) {
		this.conferenceService = conferenceService;
	}

	@RequestMapping(value = "/conferences", method = RequestMethod.GET)
	public List<Conference> getAllConferences() {
		return conferenceService.getAllConferences();
	}

	@RequestMapping(value = "/conferences", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public Conference createConference(@RequestBody CreateConference conference) {
		return conferenceService.createConference(conference);
	}

	@RequestMapping(value = "/conferences", method = RequestMethod.PUT)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public Conference updateConference(@Valid @RequestBody CreateConference conference) {
		return conferenceService.updateConference(conference);
	}

	@RequestMapping(value = "/conferences/{id}", method = RequestMethod.GET)
	public CreateConference getConference(@PathVariable Long id) {
		return conferenceService.getConference(id);
	}

	@RequestMapping(value = "/conferences/{id}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public ResponseEntity<Void> deleteConference(@PathVariable Long id) {
		conferenceService.deleteConference(id);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/conferences/closest", method = RequestMethod.GET)
	public CreateConference getClosestConference() {
		return conferenceService.getClosestConference();
	}
}


