package com.diplomka.conference.controller;

import com.diplomka.auth.security.RolesConstants;
import com.diplomka.conference.repository.entity.Article;
import com.diplomka.conference.repository.entity.Conference;
import com.diplomka.conference.repository.entity.CreateConference;
import com.diplomka.conference.repository.entity.Post;
import com.diplomka.conference.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class PostController {
	private PostService postService;

	@Autowired
	public void setClientService(PostService postService) {
		this.postService = postService;
	}

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public List<Post> getNewsOfConference(){
		return postService.getNewsOfConference();
	}

	@RequestMapping(value = "/news", method = RequestMethod.PUT)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public ResponseEntity<Void> updateNews(@RequestBody Post post) {
		postService.updateNews(post);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/news", method = RequestMethod.POST)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public Post createNews(@RequestBody Post post) {
		return postService.createNews(post);
	}
}
