package com.diplomka.conference.controller;

import com.diplomka.auth.security.RolesConstants;
import com.diplomka.conference.repository.entity.Article;
import com.diplomka.conference.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ArticleController {
	private ArticleService articleService;

	@Autowired
	public void setClientService(ArticleService articleService) {
		this.articleService = articleService;
	}

	@RequestMapping(value = "/articles/{id}", method = RequestMethod.GET)
	public List<Article> getSectionOfConference(@PathVariable Long id){
		return articleService.getArticlesOfSection(id);
	}

	@RequestMapping(value = "/articles", method = RequestMethod.PUT)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public ResponseEntity<Void> updateArticle(@RequestBody Article[] articles) {
		articleService.updateArticles(articles);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/article/{id}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public ResponseEntity<Void> deleteArticle(@PathVariable Long id) {
		articleService.deleteArticle(id);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/articlesConference", method = RequestMethod.GET)
	public List<Article> getArticlesOfConference(){
		return articleService.getArticlesOfConference();
	}

	@RequestMapping(value = "/articlesConference/{id}", method = RequestMethod.GET)
	public List<Article> getUserArticlesOfConference(@PathVariable Long id) {
		return articleService.getUserArticlesOfConference();
	}
}
