package com.diplomka.conference.controller;

import com.diplomka.auth.security.RolesConstants;
import com.diplomka.conference.repository.entity.Section;
import com.diplomka.conference.service.SectionService;
import org.apache.tomcat.jni.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class SectionController {
	private SectionService sectionService;

	@Autowired
	public void setClientService(SectionService sectionService) {
		this.sectionService = sectionService;
	}

	@RequestMapping(value = "/section", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public Section createSection(@RequestBody Section section) {
		return sectionService.createSection(section);
	}

	@RequestMapping(value = "/sections/{id}", method = RequestMethod.GET)
	public List<Section> getSectionOfConference(@PathVariable Long id, @RequestParam(value = "date") String date){
		return sectionService.getSectionsOfConference(id, date);
	}

	@RequestMapping(value = "/section/{id}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public ResponseEntity<Void> deleteSection(@PathVariable Long id) {
		sectionService.deleteSection(id);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/section", method = RequestMethod.PUT)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_ADMIN + "\")")
	public Section updateSection(@Valid @RequestBody Section section) {
		return sectionService.updateSection(section);
	}

	@RequestMapping(value = "/section/upload/{id}", method = RequestMethod.POST)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_ADMIN + "\")")
	public void uploadSectionFiles(@RequestParam("files") MultipartFile[] files, @PathVariable Long id) throws IOException {
		sectionService.uploadSectionFiles(files, id);
	}
}



