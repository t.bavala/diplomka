package com.diplomka.auth.security;

public final class RolesConstants {

	public static final long SUPERADMIN = 1;
	public static final String ROLE_SUPERADMIN = "SUPERADMIN";

	public static final long ADMIN = 2;
	public static final String ROLE_ADMIN = "ADMIN";

	public static final long USER = 3;
	public static final String ROLE_USER = "USER";

	private RolesConstants() {
	}
}
