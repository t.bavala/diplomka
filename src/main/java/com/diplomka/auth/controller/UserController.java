package com.diplomka.auth.controller;

import com.diplomka.auth.dto.RegUser;
import com.diplomka.auth.dto.UserDTO;
import com.diplomka.auth.repository.entity.User;
import com.diplomka.auth.security.RolesConstants;
import com.diplomka.auth.service.EmailSender;
import com.diplomka.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UserController {
	private UserService userService;

	@Autowired
	public void setClientService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<User> registration(@Valid @RequestBody RegUser regUser) {
		return new ResponseEntity<>(userService.registerUser(regUser, regUser.getPassword()), HttpStatus.OK);
	}

	@RequestMapping(value = "/auth", method = RequestMethod.GET)
	public ResponseEntity<Principal> user(Principal user) {
		if (userService.findByUsernameAndActivated(user.getName()) == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/account", method = RequestMethod.GET)
	public ResponseEntity<UserDTO> getAccount() {
		return new ResponseEntity<>(userService.findByUsername(), HttpStatus.OK);
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity<List<UserDTO>> getAllUsers(Pageable pageable) {
		final Page<UserDTO> page = userService.getAllUsersNoCurrent(pageable);
		return new ResponseEntity<>(page.getContent(), HttpStatus.OK);
	}

	@RequestMapping(value = "/users", method = RequestMethod.PUT)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_ADMIN + "\")")
	public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO userDTO) {
		userService.updateUser(userDTO);
		return new ResponseEntity<>(userDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/users/{username}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public ResponseEntity<Void> deleteUser(@PathVariable String username) {
		userService.deleteUser(username);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/users/roles", method = RequestMethod.GET)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public List<String> getRoles() {
		return userService.getRoles();
	}

	@RequestMapping(value = "/users/{username}", method = RequestMethod.GET)
	public ResponseEntity<UserDTO> getUser(@PathVariable String username) {
		UserDTO userDTO = Optional.of(userService.getUser(username))
				.map(UserDTO::new).orElseThrow(() -> new RuntimeException("User could not be found"));
		return new ResponseEntity<>(userDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	@PreAuthorize("hasRole(\"" + RolesConstants.ROLE_SUPERADMIN + "\")")
	public ResponseEntity<User> createUser(@Valid @RequestBody UserDTO userDTO) {
		User newUser = userService.createUser(userDTO);
		return ResponseEntity.ok()
				.body(newUser);
	}
}

