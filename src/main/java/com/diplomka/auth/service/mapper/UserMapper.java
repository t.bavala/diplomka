package com.diplomka.auth.service.mapper;

import com.diplomka.auth.dto.UserDTO;
import com.diplomka.auth.repository.entity.Role;
import com.diplomka.auth.repository.entity.User;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserMapper {

	public List<UserDTO> usersToUserDTOs(List<User> users) {
		return users.stream()
				.filter(Objects::nonNull)
				.map(this::userToUserDTO)
				.collect(Collectors.toList());
	}

	public UserDTO userToUserDTO(User user) {
		return new UserDTO(user);
	}

	public List<User> userDTOsToUsers(List<UserDTO> userDTOs) {
		return userDTOs.stream()
				.filter(Objects::nonNull)
				.map(this::userDTOToUser)
				.collect(Collectors.toList());
	}

	public User userDTOToUser(UserDTO userDTO) {
		if (userDTO == null) {
			return null;
		} else {
			User user = new User();
			user.setId(userDTO.getId());
			user.setUsername(userDTO.getUsername());
			user.setPerson(userDTO.getPerson());
			user.setPerson(userDTO.getPerson());
			user.setEmail(userDTO.getEmail());
			user.setActivated(userDTO.isActivated());
			Set<Role> roles = this.rolesFromStrings(userDTO.getRoles());
			user.setRoles(roles);
			return user;
		}
	}


	private Set<Role> rolesFromStrings(Set<String> rolesAsString) {
		Set<Role> roles = new HashSet<>();

		if(rolesAsString != null){
			roles = rolesAsString.stream().map(string -> {
				Role role = new Role();
				role.setName(string);
				return role;
			}).collect(Collectors.toSet());
		}

		return roles;
	}

	public User userFromId(Long id) {
		if (id == null) {
			return null;
		}
		User user = new User();
		user.setId(id);
		return user;
	}
}
