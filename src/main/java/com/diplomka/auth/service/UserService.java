package com.diplomka.auth.service;

import com.diplomka.auth.dto.UserDTO;
import com.diplomka.auth.repository.*;
import com.diplomka.auth.repository.entity.*;
import com.diplomka.auth.security.RolesConstants;
import com.diplomka.auth.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService {

	private UserRepository userRepository;

	private RoleRepository roleRepository;

	private PersonRepository personRepository;

	private BCryptPasswordEncoder passwordEncoder;

	private InstitutionRepository institutionRepository;

	private AddressRepository addressRepository;

	@Autowired
	private EmailSender emailSender;

	public UserService(UserRepository userRepository,
					   RoleRepository roleRepository,
					   PersonRepository personRepository,
					   InstitutionRepository institutionRepository,
					   AddressRepository addressRepository,
					   BCryptPasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.personRepository = personRepository;
		this.institutionRepository = institutionRepository;
		this.addressRepository = addressRepository;
		this.passwordEncoder = passwordEncoder;
	}

	public User registerUser(UserDTO userDTO, String password) {
		User user = userRepository.findByUsername(userDTO.getUsername());
		if (user != null)
			throw new RuntimeException("Username is already used");

		user = userRepository.findByEmail(userDTO.getEmail());
		if (user != null)
			throw new RuntimeException("Email is already used");

		User newUser = new User();
		String encryptedPassword = passwordEncoder.encode(password);
		newUser.setUsername(userDTO.getUsername());
		newUser.setPassword(encryptedPassword);
		newUser.setPerson(userDTO.getPerson());
		if (userDTO.getPerson().getInstitution().getName() == null)
			newUser.getPerson().setInstitution(null);
		newUser.setEmail(userDTO.getEmail());
		newUser.setActivated(false);
		Set<Role> roles = new HashSet<>();
		roleRepository.findById(RolesConstants.USER).ifPresent(roles::add);
		newUser.setRoles(roles);
		userRepository.save(newUser);
		return newUser;
	}

	public Optional<UserDTO> updateUser(UserDTO userDTO) {
		User existingUser = userRepository.findByEmail(userDTO.getEmail());
		if (existingUser != null && (!existingUser.getId().equals(userDTO.getId()))) {
			return null;
		}
		existingUser = userRepository.findByUsername(userDTO.getUsername());
		if (existingUser != null && (!existingUser.getId().equals(userDTO.getId()))) {
			return null;
		}

		return Optional.of(userRepository
				.findById(userDTO.getId()))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(user -> {
					user.setUsername(userDTO.getUsername());
					this.savePerson(userDTO.getPerson());
					user.setEmail(userDTO.getEmail());
					user.setActivated(userDTO.isActivated());
					Set<Role> managedRoles = user.getRoles();
					managedRoles.clear();
					for (String s : userDTO.getRoles()) {
						Role role = roleRepository.findByName(s);
						if (role != null) {
							managedRoles.add(role);
						}
					}
					return user;
				})
				.map(UserDTO::new);
	}

	private void savePerson(Person person) {
		Optional.of(personRepository
				.findById(person.getId()))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(person1 -> {
					person1.setFirstName(person.getFirstName());
					person1.setLastName(person.getLastName());
					person1.setTitle(person.getTitle());
					person1.setPhoneNumber(person.getPhoneNumber());
					if (person.getInstitution() != null && person.getInstitution().getId() != null){
						saveInstitution(person.getInstitution());
					}else{
						person1.setInstitution(person.getInstitution());
//						person1.getInstitution().setAddress(person.getInstitution().getAddress());
					}
					return person1;
				});
	}

	private void saveInstitution(Institution institution) {
		Optional.of(institutionRepository
				.findById(institution.getId()))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(institution1 -> {
					institution1.setName(institution.getName());
					this.saveAddress(institution.getAddress());
					institution1.setDic(institution.getDic());
					institution1.setIco(institution.getIco());
					return institution1;
				});
	}

	private void saveAddress(Address address) {
		Optional.of(addressRepository
				.findById(address.getId()))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(address1 -> {
					address1.setStreet(address.getStreet());
					address1.setCity(address.getCity());
					address1.setCountry(address.getCountry());
					address1.setZipCode(address.getZipCode());
					return address1;
				});
	}

	public UserDTO findByUsername() {
		String springSecurityUser = SecurityUtils.getCurrentUserLogin().get();
		User user = userRepository.findByUsername(springSecurityUser);

		if (user == null)
			return null;

		return Optional.of(user)
				.map(UserDTO::new).orElseThrow(() -> new RuntimeException("User could not be found"));
	}

	@Transactional(readOnly = true)
	public Page<UserDTO> getAllUsersNoCurrent(Pageable pageable) {
		return userRepository.findAllByUsernameNot(pageable, SecurityUtils.getCurrentUserLogin().get()).map(UserDTO::new);
	}

	public void deleteUser(String username) {
//		User user =  Optional.of(userRepository.findByUsername(username)).get();
//		Optional.of(personRepository.findById(user.getPerson().getId())).ifPresent(person -> personRepository.delete(person.get()));
		Optional.of(userRepository.findByUsername(username)).ifPresent(user1 -> userRepository.delete(user1));
	}

	public User findByUsernameAndActivated(String username) {
		return userRepository.findByUsernameAndActivatedTrue(username);
	}

	public List<String> getRoles() {
		return roleRepository.findAllByNameIsNot(RolesConstants.ROLE_SUPERADMIN).stream().map(Role::getName).collect(Collectors.toList());
	}

	public User getUser(String username) {
		return userRepository.findByUsername(username);
	}

	public User createUser(UserDTO userDTO) {
		User user = userRepository.findByUsername(userDTO.getUsername());
		if (user != null)
			throw new RuntimeException("Username is already used");

		user = userRepository.findByEmail(userDTO.getEmail());
		if (user != null)
			throw new RuntimeException("Email is already used");

		user = new User();
		user.setUsername(userDTO.getUsername());
		user.setPerson(userDTO.getPerson());
		user.getPerson().setInstitution(null);
		user.setEmail(userDTO.getEmail());

		String password = SecurityUtils.generatePassword();
		try {
			emailSender.sendSimpleMessage("","",
					"Your were registered on a website with Username: <strong>" + user.getUsername()
							+ "</strong> and Password: <strong>" + password + "</strong>");
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		String encryptedPassword = passwordEncoder.encode(password);
		user.setPassword(encryptedPassword);
		user.setActivated(true);
		Set<Role> roles = new HashSet<>();
		if (userDTO.getRoles() != null) {
			for (String s : userDTO.getRoles()) {
				Role role = roleRepository.findByName(s);
				if (role != null) {
					roles.add(role);
				}
			}
			user.setRoles(roles);
		} else {
			roleRepository.findById(RolesConstants.USER).ifPresent(roles::add);
			user.setRoles(roles);
		}
		userRepository.save(user);
		return user;
	}
}


