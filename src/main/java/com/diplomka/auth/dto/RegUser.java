package com.diplomka.auth.dto;

public class RegUser extends UserDTO {

	private String password;

	public RegUser() {
		// Empty constructor needed for Jackson.
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
