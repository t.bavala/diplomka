package com.diplomka.auth.dto;

import com.diplomka.auth.repository.entity.Person;
import com.diplomka.auth.repository.entity.Role;
import com.diplomka.auth.repository.entity.User;
import com.diplomka.configuration.Constants;

import javax.validation.constraints.*;
import java.util.Set;
import java.util.stream.Collectors;

public class UserDTO {

	private Long id;

	@NotBlank
	@Pattern(regexp = Constants.LOGIN_REGEX)
	@Size(min = 1, max = 50)
	private String username;

	private Person person;

	@Email
	@Size(min = 5, max = 254)
	private String email;

	private boolean activated = false;

	private Set<String> roles;

	public UserDTO() {
	}

	public UserDTO(User user) {
		this.id = user.getId();
		this.username = user.getUsername();
		this.person = user.getPerson();
		this.email = user.getEmail();
		this.activated = user.isActivated();
		this.roles = user.getRoles().stream()
				.map(Role::getName)
				.collect(Collectors.toSet());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}
}
