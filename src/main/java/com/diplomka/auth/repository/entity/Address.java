package com.diplomka.auth.repository.entity;

import com.diplomka.conference.repository.entity.Conference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "address")
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "street")
	private String street;

	@Column(name = "city")
	private String city;

	@Column(name = "zip_code")
	private String zipCode;

	@Column(name = "country")
	private String country;

	@OneToOne(targetEntity=Institution.class, mappedBy="address")
	@JsonIgnore
	private Institution institution;

	@OneToMany(targetEntity=Conference.class, mappedBy="address")
	@JsonIgnore
	private Set<Conference> conference;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public Set<Conference> getConference() {
		return conference;
	}

	public void setConference(Set<Conference> conference) {
		this.conference = conference;
	}
}
