package com.diplomka.auth.repository;

import com.diplomka.auth.repository.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);

	User findByEmail(String email);

	Page<User> findAllByUsernameNot(Pageable pageable, String login);

	User findByUsernameAndActivatedTrue(String username);
}
