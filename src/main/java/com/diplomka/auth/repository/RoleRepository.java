package com.diplomka.auth.repository;

import com.diplomka.auth.repository.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Role findByName(String role);

	List<Role> findAllByNameIsNot(String role);
}
