import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from "./login.service";
import {AccountService} from "../account/account.service";

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls:['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
    authenticationError: boolean;
    password: string;
    rememberMe: boolean;
    username: string;
    credentials: any;


    constructor(private accountService: AccountService, private loginService: LoginService, private router: Router) {
        this.credentials = {username: '', password: ''};
    }

    login() {
        this.loginService
            .login({
                username: this.credentials.username,
                password: this.credentials.password,
                rememberMe: this.rememberMe
            }).toPromise()
            .then(() => {
                this.accountService.identity(true)
                    .then(() => {
                        this.authenticationError = false;
                        this.router.navigate(['/']);
                    }).catch(() => {
                    this.authenticationError = true;
                });
            }).catch(() => {
            this.authenticationError = true;
        });
    }

    ngOnDestroy(): void {
        const element = document.getElementById('header');
        element.classList.remove('fixed-navbar');
    }

    ngOnInit(): void {
        const element = document.getElementById('header');
        element.classList.add('fixed-navbar');
    }
}
