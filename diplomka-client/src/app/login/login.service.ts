import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {AccountService} from "../account/account.service";
import {map} from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class LoginService {
  constructor(private accountservice: AccountService, private http: HttpClient) {
  }

  login(credentials): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers = headers.append('authorization', "Basic " + btoa(
        credentials.username + ":" + credentials.password));
    headers = headers.append("X-Requested-With", "XMLHttpRequest");
    return this.http.get('/api/auth', {headers: headers});
  }

  logout(): Observable<any> {
    this.accountservice.authenticate(null);
      return this.http.post('api/logout', {}, { observe: 'response' }).pipe(
        map((response: HttpResponse<any>) => {
          // to get a new csrf token call the api
          this.http.get('api/account').subscribe(() => {}, () => {});
          return response;
        })
      );
  }

  isSignedIn(): boolean {
    return false;
  }

}
