import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';

import {User} from './user.model';
import {ConferencesService} from "../admin/conferences/conferences.services";
import {Conference} from "../admin/conferences/conference.model";

@Injectable({providedIn: 'root'})
export class AccountService {
    private userIdentity: any;
    private authenticated = false;
    private authenticationState = new Subject<any>();
    private conference: any;

    constructor(private http: HttpClient, private conferencesService: ConferencesService) {
        this.conferencesService.findClosestConf().subscribe(
            (res: HttpResponse<Conference>) => this.conference = res.body
        );
    }

    fetch(): Observable<any> {
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.get<User>('/api/account', {headers: headers});
    }

    // save(account: any): Observable<HttpResponse<any>> {
    //   return this.http.post(SERVER_API_URL + 'api/account', account, { observe: 'response' });
    // }

    authenticate(identity) {
        this.userIdentity = identity;
        this.authenticated = identity !== null;
        this.authenticationState.next(this.userIdentity);
    }

    hasAnyRole(roles: string[]): boolean {
        if (!this.authenticated || !this.userIdentity || !this.userIdentity.roles) {
            return false;
        }

        var returnValue = false;
        for (let i = 0; i < roles.length; i++) {
            if (this.userIdentity.roles.includes(roles[i])) {
                if (roles[i] == 'ADMIN') {
                    for (let i = 0; i < this.conference.users.length; i++) {
                        if (this.conference.users[i].id == this.userIdentity.id){
                            returnValue = true;
                        }
                    }
                }else {
                    returnValue = true;
                }
            }
        }

        return returnValue;
    }

    identity(force?: boolean): Promise<any> {
        if (force) {
            this.userIdentity = undefined;
        }

        if (this.userIdentity) {
            return Promise.resolve(this.userIdentity);
        }

        return this.fetch()
            .toPromise()
            .then(response => {
                const user = response;
                if (user) {
                    this.userIdentity = user;
                    this.authenticated = true;
                } else {
                    this.userIdentity = null;
                    this.authenticated = false;
                }
                this.authenticationState.next(this.userIdentity);
                return this.userIdentity;
            })
            .catch(err => {
                this.userIdentity = null;
                this.authenticated = false;
                this.authenticationState.next(this.userIdentity);
                return null;
            });


    }

    getAuthenticationState(): Observable<any> {
        return this.authenticationState.asObservable();
    }

    isAuthenticated(): boolean {
        return this.authenticated;
    }

    getUserIdentity(): User {
        return this.userIdentity;
    }
}
