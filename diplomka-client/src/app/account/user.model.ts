import {Person} from "./person.model";

export class User {
    constructor(
        public id?: any,
        public activated?: boolean,
        public roles?: string[],
        public email?: string,
        public person?: Person,
        public username?: string
    ) {
        this.id = id ? id : null;
        this.username = username ? username : null;
        this.person = person ? person : new Person();
        this.email = email ? email : null;
        this.activated = activated ? activated : false;
        this.roles = roles ? roles : null;
    }
}
