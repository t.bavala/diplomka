import {Address} from "./address.model";

export class Institution {
    constructor(
        public id?: any,
        public name?: string,
        public ico?: string,
        public dic?: string,
        public address?: Address
    ) {
        this.id = id ? id : null;
        this.name = name ? name : null;
        this.ico = ico ? ico : null;
        this.dic = dic ? dic : null;
        this.address = address ? address : new Address();
    }
}
