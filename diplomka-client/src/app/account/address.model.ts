export class Address {
    constructor(
        public id?: any,
        public street?: string,
        public city?: string,
        public zipCode?: string,
        public country?: string,
    ) {
        this.id = id ? id : null;
        this.street = street ? street : null;
        this.city = city ? city : null;
        this.zipCode = zipCode ? zipCode : null;
        this.country = country ? country : null;
    }
}
