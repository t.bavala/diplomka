import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {User} from "./user.model";
import {HttpResponse} from "@angular/common/http";
import {UserService} from "../admin/userManagement/user.service";
import {Article} from "../admin/conferences/conference/section/article/article.model";

@Component({
    selector: 'app-search-users',
    templateUrl: './searchUser.component.html'
})
export class SearchUserComponent implements OnInit {

    @Output() selectedUser = new EventEmitter();
    @Input() article: Article;
    @Input() usersOfElement: User[];
    users: User[];
    private searchUsers: User[];
    searchInput: any;
    private searchShow: boolean;

    constructor( private userService: UserService) {
    }

    ngOnInit(): void {
        this.loadAll();
        if (this.article.user != null)
            this.searchInput = this.article.user.username;
    }

    loadAll() {
        this.userService
            .query({
            })
            .subscribe(
                (res: HttpResponse<User[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpResponse<any>) => this.onError(res.body)
            );
    }

    private onSuccess(data, headers) {
        const allUsers = data;
        this.users = [];
        this.searchUsers = [];
        for (let i = 0; i < allUsers.length; i++) {
            if (this.usersOfElement.indexOf(allUsers[i]) == -1){
                this.users.push(allUsers[i]);
                this.searchUsers.push(allUsers[i]);
            }
        }
    }

    private onError(error) {
    }

    myFunction() {
        const users = [];
        if (this.searchInput === '') {
            this.searchUsers = this.users;
        } else {
            for (let i = 0; i < this.users.length; i++) {
                if ((this.users[i].username).indexOf(this.searchInput) > -1) {
                    users.push(this.users[i]);
                }
            }
            this.searchUsers = users;
        }
    }

    searchUsersShow() {
        this.searchShow = true;
        if (this.article.user != null){
            this.searchInput = this.article.user.username;
            this.myFunction();
        }
    }

    searchUsersHide() {
        this.searchShow = false;
    }

    selectUser(user: User) {
        this.article.user = user;
        console.log(user);
        this.selectedUser.emit(this.article);
        this.loadAll();
        this.searchInput = this.article.user.username;
        this.searchShow = false;
    }
}
