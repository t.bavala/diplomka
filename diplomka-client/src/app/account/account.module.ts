import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {SharedModule} from "../shared/shared.module";
import {SettingsComponent} from "./settings/settings.component";
import {accountState} from "./account.route";
import { UserArticlesComponent } from './user-articles/user-articles.component';
import {ConferencesModule} from "../admin/conferences/conferences.module";
import {ArticleUpdateComponent} from "../admin/conferences/conference/section/article/articleUpdate.component";


@NgModule({
    imports: [SharedModule, RouterModule.forChild(accountState), ConferencesModule],
    declarations: [
        SettingsComponent,
        UserArticlesComponent,
        ArticleUpdateComponent
    ],
    entryComponents: [
        ArticleUpdateComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccountModule {}
