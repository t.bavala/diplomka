import {Institution} from "./institution.model";

export class Person {
    constructor(
        public id?: any,
        public title?: string,
        public firstName?: string,
        public lastName?: string,
        public phoneNumber?: string,
        public institution?: Institution
    ) {
        this.id = id ? id : null;
        this.title = title ? title : null;
        this.firstName = firstName ? firstName : null;
        this.lastName = lastName ? lastName : null;
        this.phoneNumber = phoneNumber ? phoneNumber : null;
        this.institution = institution ? institution : new Institution();
    }
}
