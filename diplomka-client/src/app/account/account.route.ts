import { Routes } from '@angular/router';
import {settingsRoute} from "./settings/settings.route";
import {userArticlesRoute} from "./user-articles/user-articles.route";

const ACCOUNT_ROUTES = [
    settingsRoute,
    userArticlesRoute
];

export const accountState: Routes = [
    {
        path: '',
        children: ACCOUNT_ROUTES
    }
];
