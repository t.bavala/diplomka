import {ActivatedRouteSnapshot, CanActivate, Route, Router, RouterStateSnapshot} from '@angular/router';

import {Injectable} from "@angular/core";
import {AccountService} from "../account.service";
import {UserArticlesComponent} from "./user-articles.component";

@Injectable({ providedIn: 'root' })
export class AccountResolve implements CanActivate {
    constructor(private accountService: AccountService, private router: Router) {}

    canActivate(route : ActivatedRouteSnapshot , state : RouterStateSnapshot): Promise<boolean> {
        return this.accountService.identity().then(
            account => {
                if(this.accountService.hasAnyRole(['ADMIN','USER'])){
                    return true;
                } else {
                    this.router.navigate(['/']);
                    return false;
                }
            }
        );
    }
}

export const userArticlesRoute: Route = {
    path: 'user-articles',
    component: UserArticlesComponent,
    data: {
        authorities: ['USER']
    },
    canActivate: [AccountResolve]
};
