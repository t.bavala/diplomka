import {Component, OnDestroy, OnInit} from '@angular/core';
import {Article} from "../../admin/conferences/conference/section/article/article.model";
import {ArticleService} from "../../admin/conferences/conference/section/article/article.service";
import {HttpResponse} from "@angular/common/http";
import {ArticleDeleteDialogComponent} from "../../admin/conferences/conference/section/article/articleDeleteDialog.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ArticleUpdateComponent} from "../../admin/conferences/conference/section/article/articleUpdate.component";
import {AccountService} from "../account.service";

@Component({
  selector: 'app-user-articles',
  templateUrl: './user-articles.component.html',
  styleUrls: ['./user-articles.component.scss']
})
export class UserArticlesComponent implements OnInit, OnDestroy {
    articles: Article[];

  constructor(private articleService: ArticleService, private modalService: NgbModal, private accountService: AccountService) { }

    ngOnInit() {
      const element = document.getElementById('header');
      element.classList.add('fixed-navbar');

        if(this.accountService.hasAnyRole(['ADMIN','SUPERADMIN'])){
            this.articleService.getAll().subscribe(
                (res: HttpResponse<Article[]>) => this.articles = res.body
            );
        } else if (this.accountService.hasAnyRole(['USER'])){
            this.articleService.getUserArticles(this.accountService.getUserIdentity().id).subscribe(
                (res: HttpResponse<Article[]>) => this.articles = res.body
            );
        }
    }

    ngOnDestroy(): void {
        const element = document.getElementById('header');
        element.classList.remove('fixed-navbar');
    }

     updateArticle(article: Article) {
        if (article.id != null) {
            const modalRef = this.modalService.open(ArticleUpdateComponent, { size: 'lg', backdrop: 'static' });
            modalRef.componentInstance.article = article;
            modalRef.result.then(
                result => {
                },
                reason => {
                }
            );
        }
    }
}
