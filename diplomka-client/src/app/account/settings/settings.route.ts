import {ActivatedRouteSnapshot, CanActivate, Route, Router, RouterStateSnapshot} from '@angular/router';

import { SettingsComponent } from './settings.component';
import {Injectable} from "@angular/core";
import {AccountService} from "../account.service";

@Injectable({ providedIn: 'root' })
export class AccountResolve implements CanActivate {
    constructor(private accountService: AccountService, private router: Router) {}

    canActivate(route : ActivatedRouteSnapshot , state : RouterStateSnapshot): Promise<boolean> {
        return this.accountService.identity().then(
            account => {
                if(this.accountService.hasAnyRole(['ADMIN','USER'])){
                    return true;
                } else {
                    this.router.navigate(['/']);
                    return false;
                }
            }
        );
    }
}

export const settingsRoute: Route = {
    path: 'settings',
    component: SettingsComponent,
    data: {
        authorities: ['USER']
    },
    canActivate: [AccountResolve]
};
