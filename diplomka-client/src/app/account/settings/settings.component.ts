import {Component, OnDestroy, OnInit} from '@angular/core';
import {AccountService} from "../account.service";
import {UserService} from "../../admin/userManagement/user.service";
import {Institution} from "../institution.model";

@Component({
    selector: 'jhi-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
    error: string;
    success: string;
    settingsAccount: any;

    constructor(
        private accountService: AccountService,
        private userService: UserService
    ) {}

    ngOnInit() {
        const element = document.getElementById('header');
        element.classList.add('fixed-navbar');
        this.accountService.identity().then(account => {
            this.settingsAccount = account;
            if(this.settingsAccount.person.institution == null){
                this.settingsAccount.person.institution = new Institution();
            }
        });
    }

    save() {
        this.userService.update(this.settingsAccount).subscribe(
            () => {
                this.error = null;
                this.success = 'OK';
                this.accountService.identity(true).then(account => {
                    this.settingsAccount = account;
                });
            },
            () => {
                this.success = null;
                this.error = 'ERROR';
            }
        );
    }

    ngOnDestroy(): void {
        const element = document.getElementById('header');
        element.classList.remove('fixed-navbar');
    }

    // copyAccount(account) {
    //     return {
    //         activated: account.activated,
    //         email: account.email,
    //         firstName: account.firstName,
    //         lastName: account.lastName,
    //         username: account.username,
    //         person: account.person
    //     };
    // }
}
