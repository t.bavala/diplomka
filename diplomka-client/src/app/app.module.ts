import './vendor.ts';

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {
    MainComponent,
    NavbarComponent,
    FooterComponent,
    HasAnyRoleDirective
} from './layouts';
import {LoginComponent} from "./login/login.component";
import {HttpClientModule} from '@angular/common/http';
import {RegisterComponent} from "./register/register.component";
import {UserMgmtDeleteDialogComponent} from "./admin";
import {SharedModule} from "./shared/shared.module";
import {NgbDatepickerConfig} from "@ng-bootstrap/ng-bootstrap";
import { HomeComponent } from './home/home.component';
import {AccountModule} from "./account/account.module";
import { NewsComponent } from './news/news.component';
import {AgmCoreModule} from "@agm/core";
import {ConferencesModule} from "./admin/conferences/conferences.module";
import {RouterModule} from "@angular/router";
import {conferencesRoute} from "./admin/conferences/conferences.route";
import {newsRoute} from "./news/news.route";
import {NewsUpdateComponent} from "./news/newsUpdate.component";

@NgModule({
    declarations: [
        MainComponent,
        NavbarComponent,
        FooterComponent,
        LoginComponent,
        RegisterComponent,
        UserMgmtDeleteDialogComponent,
        HomeComponent,
        NewsComponent,
        NewsUpdateComponent
    ],
    imports: [
        RouterModule.forChild(newsRoute),
        BrowserModule,
        AppRoutingModule,
        SharedModule.forRoot(),
        HttpClientModule,
        AccountModule,
        ConferencesModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyCt3vaBDTtMMC_CDlbBfLRed1HgXLM6meA'
        })
    ],
    exports: [HasAnyRoleDirective],
    // providers: [AppService],
    bootstrap: [MainComponent],
    entryComponents: [UserMgmtDeleteDialogComponent]
})
export class AppModule {
    constructor(private dpConfig: NgbDatepickerConfig){}
}
