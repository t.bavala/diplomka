import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {navbarRoute} from "./layouts";
import {loginRoute} from "./login/login.route";
import {registerRoute} from "./register/register.route";
import {homeRoute} from "./home/home.route";

const routes: Routes = [navbarRoute, loginRoute, registerRoute, homeRoute];

@NgModule({
    imports: [
        RouterModule.forRoot(
            [
                {
                    path: 'admin',
                    loadChildren: './admin/admin.module#AdminModule'
                },
                {
                    path: 'conferences',
                    loadChildren: './admin/conferences/conferences.module#ConferencesModule'
                },
                ...routes
            ]
        )
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
