import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {AccountService} from "../../account/account.service";

@Directive({
    selector: '[hasAnyRole]'
})
export class HasAnyRoleDirective {
    private roles: string[];

    constructor(
        private accountService: AccountService,
        private templateRef: TemplateRef<any>,
        private viewContainerRef: ViewContainerRef
    ) {
    }

    @Input()
    set hasAnyRole(value: string | string[]) {
        this.roles = typeof value === 'string' ? [value] : value;
        this.updateView();
        this.accountService.getAuthenticationState().subscribe(identity => this.updateView());
    }

    private updateView(): void {
        const hasAnyRole = this.accountService.hasAnyRole(this.roles);
        this.viewContainerRef.clear();
        if (hasAnyRole) {
            this.viewContainerRef.createEmbeddedView(this.templateRef);
        }
    }
}
