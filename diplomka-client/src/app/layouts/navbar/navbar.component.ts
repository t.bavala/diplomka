import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {LoginService} from 'src/app/login/login.service';
import {AccountService} from '../../account/account.service';
import {User} from '../../account/user.model';
import {ConferencesService} from '../../admin/conferences/conferences.services';
import {HttpResponse} from '@angular/common/http';
import {Conference} from '../../admin/conferences/conference.model';

@Component({
    selector: 'navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['navbar.scss']
})
export class NavbarComponent implements OnInit {
    user: User;
    isNavbarCollapsed: boolean;
    closestConference: Conference;

    constructor(
        private loginService: LoginService,
        private accountService: AccountService,
        private conferenceesService: ConferencesService,
        private router: Router
    ) {
        this.isNavbarCollapsed = true;
        this.closestConference = new Conference();
        this.closestConference.id = 0;
    }

    ngOnInit() {
        this.accountService.identity().then((user: User) => {
            this.user = user;
        });
        this.getClosestConf();
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout().subscribe();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getClosestConf() {
        this.conferenceesService.findClosestConf().subscribe(
            (res: HttpResponse<Conference>) => this.closestConference = res.body
        );
    }

    clickMenu() {
        this.conferenceesService.findClosestConf().subscribe(
            (res: HttpResponse<Conference>) => this.onSuccess(res.body) ,
        );
    }


    private onSuccess(conference: Conference) {
        this.closestConference = conference;
        // this.router.navigate(['/conferences/' + conference.id + '/view']);
    }

    setActive(e) {
        e.classList.add('active');
    }
}
