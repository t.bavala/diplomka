import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, NavigationError } from '@angular/router';
import { AccountService } from "../../account/account.service";

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['main.scss']
})
export class MainComponent implements OnInit {
  constructor(private accountService: AccountService, private router: Router) {
  }

  private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
    let title: string = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : 'diplomka';
    if (routeSnapshot.firstChild) {
      title = this.getPageTitle(routeSnapshot.firstChild) || title;
    }
    return title;
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationError && event.error.status === 404) {
        this.router.navigate(['/404']);
      }
    });
  }
}
