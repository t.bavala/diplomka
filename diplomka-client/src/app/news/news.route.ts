import {ActivatedRouteSnapshot, Resolve, Route, RouterStateSnapshot, Routes} from '@angular/router';
import {Injectable} from "@angular/core";
import {NewsService} from "./news.service";
import {NewsComponent} from "./news.component";
import {Post} from "./post.model";
import {NewsUpdateComponent} from "./newsUpdate.component";

@Injectable({providedIn: 'root'})
export class NewsResolve implements Resolve<any> {
    constructor(private service: NewsService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id);
        }
        return new Post();
    }
}

export const newsRoute: Routes = [
    {
        path: 'news',
        component: NewsComponent
    },
    {
        path: 'news/new',
        component: NewsUpdateComponent,
        resolve: {
            conference: NewsResolve
        }
    },
    {
        path: 'news/:id/edit',
        component: NewsUpdateComponent,
        resolve: {
            conference: NewsResolve
        }
    }
];
