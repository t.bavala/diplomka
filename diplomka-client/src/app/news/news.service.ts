import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {Post} from "./post.model";

@Injectable({providedIn: 'root'})
export class NewsService {
    public resourceUrl = 'api/news';

    constructor(private http: HttpClient) {}

    query(): Observable<HttpResponse<Post[]>> {
        const options = new HttpParams();
        return this.http.get<Post[]>(this.resourceUrl, {params: options, observe: 'response'});
    }

    create(post: Post): Observable<HttpResponse<Post>> {
        return this.http.post<Post>(this.resourceUrl, post, { observe: 'response' });
    }

    update(post: Post): Observable<HttpResponse<Post>> {
        return this.http.put<Post>(this.resourceUrl, post, { observe: 'response' });
    }

    find(id: any): Observable<HttpResponse<Post>> {
        return this.http.get<Post>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    delete(id: any): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
