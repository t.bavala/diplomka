import {Component, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Post} from "./post.model";
import {NewsService} from "./news.service";
import {User} from "../account/user.model";
import {HttpResponse} from "@angular/common/http";
import {UserService} from "../admin/userManagement/user.service";

@Component({
    selector: 'newsUpdate',
    templateUrl: './newsUpdate.component.html'
})
export class NewsUpdateComponent implements OnInit{
    post: Post;
    isSaving: boolean;
    users: User[];
    private searchUsers: User[];
    searchInput: any;
    private searchShow: boolean;

    constructor(private newsService: NewsService,
                public activeModal: NgbActiveModal,
                private userService: UserService) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save(post) {
        this.isSaving = true;
        if (this.post.id !== null) {
            this.newsService.update(this.post).subscribe(response => this.activeModal.dismiss(post));
        } else {
            this.newsService.create(this.post).subscribe(response => this.activeModal.dismiss(post));
        }
    }

    ngOnInit(): void {
        this.isSaving = false;
        this.loadAll();
        if (this.post.user != null)
            this.searchInput = this.post.user.username;
    }

    loadAll() {
        this.userService
            .query({
            })
            .subscribe(
                (res: HttpResponse<User[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpResponse<any>) => this.onError(res.body)
            );
    }
    private onSuccess(data, headers) {
        const allUsers = data;
        this.users = [];
        this.searchUsers = [];
        for (let i = 0; i < allUsers.length; i++) {
            this.users.push(allUsers[i]);
            this.searchUsers.push(allUsers[i]);
        }
    }

    private onError(error) {
    }

    myFunction() {
        const users = [];
        if (this.searchInput === '') {
            this.searchUsers = this.users;
        } else {
            for (let i = 0; i < this.users.length; i++) {
                if ((this.users[i].username).indexOf(this.searchInput) > -1) {
                    users.push(this.users[i]);
                }
            }
            this.searchUsers = users;
        }
    }

    searchUsersShow() {
        this.searchShow = true;
        if (this.post.user != null){
            this.searchInput = this.post.user.username;
            this.myFunction();
        }
    }

    searchUsersHide() {
        this.searchShow = false;
    }

    selectUser(user: User) {
        this.post.user = user;
        this.loadAll();
        this.searchInput = this.post.user.username;
        this.searchShow = false;
    }
}
