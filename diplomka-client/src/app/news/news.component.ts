import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SectionUpdateComponent} from "../admin/conferences/conference/section/sectionUpdate.component";
import {Section} from "../admin/conferences/conference/section/section.model";
import {Post} from "./post.model";
import {HttpResponse} from "@angular/common/http";
import {NewsService} from "./news.service";
import {NewsUpdateComponent} from "./newsUpdate.component";

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit, OnDestroy {

    routeData: any;
    news: Post[];

    constructor(private activatedRoute: ActivatedRoute,
                private modalService: NgbModal,
                private newsService: NewsService) {
        this.routeData = this.activatedRoute.data.subscribe(data => {
        });
    }

    ngOnInit() {
        const element = document.getElementById('header');
        element.classList.add('fixed-navbar');
    }

    ngOnDestroy(): void {
        this.routeData.unsubscribe();
        const element = document.getElementById('header');
        element.classList.remove('fixed-navbar');
    }

    createNews(post: any) {
        const modalRef = this.modalService.open(NewsUpdateComponent, {size: 'lg', backdrop: 'static'});
        if (post != null) {
            modalRef.componentInstance.section = post;
        } else {
            modalRef.componentInstance.post = new Post();
        }

        modalRef.result.then(
            result => {

            },
            reason => {
                if (reason.body) {
                    this.loadAll();
                }
            }
        );
    }

    loadAll() {
        this.newsService.query().subscribe(
            (res: HttpResponse<Section[]>) => this.onSuccess(res.body),
            (res: HttpResponse<any>) => this.onError(res.body)
        );
    }

    private onSuccess(data) {
        this.news = data;
    }

    private onError(body: any) {

    }
}
