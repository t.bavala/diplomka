import {User} from "../account/user.model";
import {Conference} from "../admin/conferences/conference.model";

export class Post {
    constructor(
        public id?: any,
        public postText?: string,
        public createdAt?: Date,
        public user?: User,
        public conference?: Conference,
    ) {
        this.id = id ? id : null;
        this.postText = postText ? postText : null;
        this.createdAt = createdAt ? createdAt : null;
        this.user = user ? user : null;
        this.conference = conference ? conference : null;
    }
}
