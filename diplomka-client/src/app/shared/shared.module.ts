import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import {SharedLibsModule} from "./sharedlibs.module";
import {HasAnyRoleDirective} from "../layouts";
import {SearchUserComponent} from "../account/searchUser.component";

@NgModule({
    imports: [SharedLibsModule],
    declarations: [HasAnyRoleDirective, SearchUserComponent],
    exports: [SharedLibsModule, HasAnyRoleDirective, SearchUserComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {
    static forRoot() {
        return {
            ngModule: SharedModule
        };
    }
}
