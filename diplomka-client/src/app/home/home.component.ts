import {Component, HostListener, OnInit} from '@angular/core';
import {HttpResponse} from "@angular/common/http";
import {Conference} from "../admin/conferences/conference.model";
import {ConferencesService} from "../admin/conferences/conferences.services";

@Component({
    selector: 'appHome',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    closestConference: Conference;
    timeBetweenDates: number;
    confDays: any;

    constructor(private conferencesService: ConferencesService) {
    }

    ngOnInit() {
        this.getClosestConf();
    }

    @HostListener('window:scroll', ['$event'])
    onWindowScroll(e) {
        if (window.pageYOffset > 50) {
            const element = document.getElementById('header');
            element.classList.add('fixed-navbar');
        } else {
            const element = document.getElementById('header');
            element.classList.remove('fixed-navbar');
        }
    }

    getClosestConf() {
        this.conferencesService.findClosestConf().subscribe(
            (res: HttpResponse<Conference>) => this.success(res.body)
        );
    }

    private success(body: Conference) {
        this.closestConference = body;
        this.confDays = this.conferenceDays();
        this.timeBetweenDates = new Date(this.closestConference.dateFrom).getTime() - new Date().getTime();
        setInterval(() => {
            if(this.timeBetweenDates > 0) {
                this.timeBetweenDates = this.timeBetweenDates - 1000;

            }
        },1000)
    }

    conferenceDays() {
        const dateArray = [];
        const currentDate = new Date(this.closestConference.dateFrom);
        const dateTo = new Date(this.closestConference.dateTo);
        while (currentDate <= dateTo) {
            dateArray.push(new Date(currentDate));
            currentDate.setDate(currentDate.getDate() + 1);
        }
        return dateArray;
    }
}
