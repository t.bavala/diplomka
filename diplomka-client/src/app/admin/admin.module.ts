import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import {
    UserManagementComponent, UserMgmtUpdateComponent, UserMgmtDetailComponent
} from './';
import {RouterModule} from "@angular/router";
import {adminState} from "./admin.route";
import {SharedModule} from "../shared/shared.module";

@NgModule({
    imports: [
        RouterModule.forChild(adminState),
        SharedModule
    ],
    declarations: [
        UserManagementComponent,
        UserMgmtUpdateComponent,
        UserMgmtDetailComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdminModule {
    constructor() {
    }
}
