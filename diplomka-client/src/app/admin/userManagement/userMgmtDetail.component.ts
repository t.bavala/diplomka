import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {User} from "../../account/user.model";

@Component({
    selector: 'userMgmtDetail',
    templateUrl: './userMgmtDetail.component.html'
})
export class UserMgmtDetailComponent implements OnInit, OnDestroy {
    user: User;

    constructor(private route: ActivatedRoute) {}

    ngOnInit() {
        const element = document.getElementById('header');
        element.classList.add('fixed-navbar');
        this.route.data.subscribe(({ user }) => {
            this.user = user.body ? user.body : user;
        });
    }

    ngOnDestroy(): void {
        const element = document.getElementById('header');
        element.classList.remove('fixed-navbar');
    }
}
