import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import {User} from "../../account/user.model";

@Injectable({ providedIn: 'root' })
export class UserService {
    public resourceUrl = 'api/users';

    constructor(private http: HttpClient) {}

    query(req?: any): Observable<HttpResponse<User[]>> {
        const options = new HttpParams();
        return this.http.get<User[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    update(user: User): Observable<HttpResponse<User>> {
        return this.http.put<User>(this.resourceUrl, user, { observe: 'response' });
    }

    delete(username: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${username}`, { observe: 'response' });
    }

    roles(): Observable<string[]> {
        return this.http.get<string[]>('api/users/roles');
    }

    create(user: User): Observable<HttpResponse<User>> {
        return this.http.post<User>(this.resourceUrl, user, { observe: 'response' });
    }

    find(login: string): Observable<HttpResponse<User>> {
        return this.http.get<User>(`${this.resourceUrl}/${login}`, { observe: 'response' });
    }
}
