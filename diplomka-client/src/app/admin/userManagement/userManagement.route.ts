import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {UserManagementComponent} from "./userManagement.component";
import {UserMgmtUpdateComponent} from "./userMgmtUpdate.component";
import {Injectable} from "@angular/core";
import {UserService} from "./user.service";
import {User} from "../../account/user.model";
import {UserMgmtDetailComponent} from "./userMgmtDetail.component";

@Injectable({providedIn: 'root'})
export class UserMgmtResolve implements Resolve<any> {
    constructor(private service: UserService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['username'] ? route.params['username'] : null;
        if (id) {
            return this.service.find(id);
        }
        return new User();
    }
}

export const userManagementRoute: Routes = [
    {
        path: 'userManagement',
        component: UserManagementComponent,
        data: {
            pageTitle: 'userManagement'
        }
    },
    {
        path: 'userManagement/new',
        component: UserMgmtUpdateComponent,
        resolve: {
            user: UserMgmtResolve
        }
    },
    {
        path: 'userManagement/:username/edit',
        component: UserMgmtUpdateComponent,
        resolve: {
            user: UserMgmtResolve
        }
    },
    {
        path: 'userManagement/:username/view',
        component: UserMgmtDetailComponent,
        resolve: {
            user: UserMgmtResolve
        }
    }
];
