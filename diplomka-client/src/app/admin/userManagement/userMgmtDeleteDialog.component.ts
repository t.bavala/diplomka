import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {User} from '../../account/user.model';
import {UserService} from './user.service';

@Component({
    selector: 'userMgmtDeleteDialog',
    templateUrl: './userMgmtDeleteDialog.component.html'
})
export class UserMgmtDeleteDialogComponent {
    user: User;

    constructor(private userService: UserService, public activeModal: NgbActiveModal) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(username) {
        this.userService.delete(username).subscribe(response => {
            this.activeModal.dismiss(username);
        });
    }
}
