import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';

import {UserService} from './user.service';
import {AccountService} from '../../account/account.service';
import {User} from '../../account/user.model';
import {ActivatedRoute} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserMgmtDeleteDialogComponent} from './userMgmtDeleteDialog.component';

@Component({
    selector: 'userManagement',
    templateUrl: './userManagement.component.html'
})
export class UserManagementComponent implements OnInit, OnDestroy {
    currentAccount: any;
    users: User[];
    error: any;
    success: any;
    routeData: any;
    links: any;
    totalItems: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    private searchUsers: User[];
    searchInput: any;
    private searchShow: boolean;

    constructor(
        private userService: UserService,
        private accountService: AccountService,
        private activatedRoute: ActivatedRoute,
        private modalService: NgbModal
    ) {
        this.routeData = this.activatedRoute.data.subscribe(data => {
        });
    }

    ngOnInit() {
        const element = document.getElementById('header');
        element.classList.add('fixed-navbar');
        this.accountService.identity().then(account => {
            this.currentAccount = account;
            this.loadAll();
        });

    }

    ngOnDestroy() {
        this.routeData.unsubscribe();
        const element = document.getElementById('header');
        element.classList.remove('fixed-navbar');
    }

    loadAll() {
        this.userService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                // sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<User[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpResponse<any>) => this.onError(res.body)
            );
    }

    trackIdentity(index, item: User) {
        return item.id;
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.users = data;
        this.searchUsers = data;
    }

    private onError(error) {
    }

    setActive(user, isActivated) {
        user.activated = isActivated;

        this.userService.update(user).subscribe(response => {
            if (response.status === 200) {
                this.error = null;
                this.success = 'OK';
                this.loadAll();
            } else {
                this.success = null;
                this.error = 'ERROR';
            }
        });
    }
    deleteUser(user: User) {
        const modalRef = this.modalService.open(UserMgmtDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.user = user;
        modalRef.result.then(
            result => {
            },
            reason => {
                if (reason != 'cancel') {
                    const index = this.users.indexOf(user);
                    this.users.splice(index, 1);
                }
            }
        );
    }
    myFunction() {
        const users = [];
        if (this.searchInput === '') {
            this.searchUsers = this.users;
        } else {
            for (let i = 0; i < this.users.length; i++) {
                if ((this.users[i].username).indexOf(this.searchInput) > -1) {
                    users.push(this.users[i]);
                }
            }
            this.searchUsers = users;
        }
    }

    searchUsersShow() {
        this.searchShow = true;
    }

    searchUsersHide() {
        this.searchShow = false;
    }
}
