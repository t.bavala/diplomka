import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {User} from "../../account/user.model";
import {UserService} from "./user.service";

@Component({
    selector: 'userMgmtUpdate',
    templateUrl: './userMgmtUpdate.component.html'
})
export class UserMgmtUpdateComponent implements OnInit, OnDestroy {
    user: User;
    roles: any[];
    isSaving: boolean;

    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit() {
        const element = document.getElementById('header');
        element.classList.add('fixed-navbar');
        this.isSaving = false;
        this.route.data.subscribe(({ user }) => {
            this.user = user.body ? user.body : user;
        });
        this.roles = [];
        this.userService.roles().subscribe(roles => {
            this.roles = roles;
        });
    }

    ngOnDestroy(){
        const element = document.getElementById('header');
        element.classList.remove('fixed-navbar');
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.user.id !== null) {
            this.userService.update(this.user).subscribe(response => this.onSaveSuccess(response), (response) => this.onSaveError(response));
        } else {
            this.userService.create(this.user).subscribe(response => this.onSaveSuccess(response), (response) => this.onSaveError(response));
        }
    }

    private onSaveSuccess(result) {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError(result) {
        this.isSaving = false;
    }
}
