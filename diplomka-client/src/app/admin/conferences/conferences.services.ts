import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {Conference} from "./conference.model";

@Injectable({providedIn: 'root'})
export class ConferencesService {
    public resourceUrl = 'api/conferences';

    constructor(private http: HttpClient) {}

    query(): Observable<HttpResponse<Conference[]>> {
        const options = new HttpParams();
        return this.http.get<Conference[]>(this.resourceUrl, {params: options, observe: 'response'});
    }

    create(conference: Conference): Observable<HttpResponse<Conference>> {
        return this.http.post<Conference>(this.resourceUrl, conference, { observe: 'response' });
    }

    update(conference: Conference): Observable<HttpResponse<Conference>> {
        return this.http.put<Conference>(this.resourceUrl, conference, { observe: 'response' });
    }

    find(id: any): Observable<HttpResponse<Conference>> {
        return this.http.get<Conference>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    delete(id: any): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    findClosestConf(): Observable<HttpResponse<Conference>> {
        return this.http.get<Conference>('api/conferences/closest', {observe: 'response'});
    }
}
