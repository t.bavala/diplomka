import {Component, OnInit, OnDestroy} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {Conference} from "./conference.model";
import {ConferencesService} from "./conferences.services";
import {ActivatedRoute} from "@angular/router";
import {ConferenceDeleteDialogComponent} from "./conference/conferenceDeleteDialog.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'conferences',
    templateUrl: './conferences.component.html'
})
export class ConferencesComponent implements OnInit, OnDestroy {
    currentAccount: any;
    conferences: Conference[];
    error: any;
    success: any;
    routeData: any;

    constructor(
        private conferencesService: ConferencesService,
        private activatedRoute: ActivatedRoute,
        private modalService: NgbModal
    ) {
    }


    ngOnInit() {
        const element = document.getElementById('header');
        element.classList.add('fixed-navbar');
        this.routeData = this.activatedRoute.data.subscribe(data => {
        });
        this.loadAll();
    }

    ngOnDestroy() {
        this.routeData.unsubscribe();
        const element = document.getElementById('header');
        element.classList.remove('fixed-navbar');
    }

    loadAll() {
        this.conferencesService
            .query()
            .subscribe(
                (res: HttpResponse<Conference[]>) => this.onSuccess(res.body),
                (res: HttpResponse<any>) => this.onError(res.body)
            );
    }

    trackIdentity(index, item: Conference) {
        return item.id;
    }

    private onSuccess(data) {
        this.conferences = data;
    }

    private onError(error) {
    }

    deleteConference(conference: Conference) {
        const modalRef = this.modalService.open(ConferenceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.conference = conference;
        modalRef.result.then(
            result => {
            },
            reason => {
                if (reason != 'cancel'){
                    let index = this.conferences.indexOf(conference);
                    this.conferences.splice(index,1);
                }
            }
        );
    }
}
