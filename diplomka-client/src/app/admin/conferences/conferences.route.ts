import {ActivatedRouteSnapshot, Resolve, Route, RouterStateSnapshot, Routes} from '@angular/router';
import {ConferencesComponent} from "./conferences.component";
import {Injectable} from "@angular/core";
import {ConferenceUpdateComponent} from "./conference/conferenceUpdate.component";
import {ConferencesService} from "./conferences.services";
import {Conference} from "./conference.model";
import {ConferenceComponent} from "./conference/conference.component";

@Injectable({providedIn: 'root'})
export class ConferenceResolve implements Resolve<any> {
    constructor(private service: ConferencesService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id);
        }
        return new Conference();
    }
}

export const conferencesRoute: Routes = [
    {
        path: '',
        component: ConferencesComponent
    },
    {
        path: 'new',
        component: ConferenceUpdateComponent,
        resolve: {
            conference: ConferenceResolve
        }
    },
    {
        path: ':id/edit',
        component: ConferenceUpdateComponent,
        resolve: {
            conference: ConferenceResolve
        }
    },
    {
        path: ':id/view',
        component: ConferenceComponent,
        resolve: {
            conference: ConferenceResolve
        }
    }
];
