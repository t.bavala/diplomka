import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Conference} from '../conference.model';

@Component({
    selector: 'conference',
    templateUrl: './conference.component.html'
})
export class ConferenceComponent implements OnInit, OnDestroy {
    conference: Conference;
    confDays: any;

    constructor(private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.data.subscribe(({ conference }) => {
            this.conference = conference.body ? conference.body : conference;
        });

        this.confDays = this.conferenceDays();

        const element = document.getElementById('header');
        element.classList.add('fixed-navbar');
    }

    conferenceDays() {
        const dateArray = [];
        const currentDate = new Date(this.conference.dateFrom);
        const dateTo = new Date(this.conference.dateTo);
        while (currentDate <= dateTo) {
            dateArray.push(new Date(currentDate));
            currentDate.setDate(currentDate.getDate() + 1);
        }
        return dateArray;
    }

    ngOnDestroy(): void {
        const element = document.getElementById('header');
        element.classList.remove('fixed-navbar');
    }
}
