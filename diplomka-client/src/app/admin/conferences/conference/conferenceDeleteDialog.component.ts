import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Conference} from "../conference.model";
import {ConferencesService} from "../conferences.services";

@Component({
    selector: 'conferenceDeleteDialog',
    templateUrl: './conferenceDeleteDialog.component.html'
})
export class ConferenceDeleteDialogComponent {
    conference: Conference;

    constructor(private conferencesService: ConferencesService, public activeModal: NgbActiveModal) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(conference) {
        this.conferencesService.delete(conference.id).subscribe(response => {
            this.activeModal.dismiss(conference);
        });
    }
}
