import {Component, Input, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SectionComponent} from '../section/section.component';
import {SectionUpdateComponent} from '../section/sectionUpdate.component';
import {Section} from '../section/section.model';
import {SectionService} from '../section/section.service';
import {HttpResponse} from '@angular/common/http';
import {SectionDeleteDialogComponent} from '../section/sectionDeleteDialog.component';
import {formatDate} from '@angular/common';

@Component({
    selector: 'app-conference-day',
    templateUrl: './conference-day.component.html',
    styleUrls: ['./conference-day.component.scss']
})
export class ConferenceDayComponent implements OnInit {

    @Input() dateOfDay;
    @Input() conference;
    times: any;
    rooms: string[];
    roomsWidth: any;
    sections: Section[];

    constructor(private sectionService: SectionService, private modalService: NgbModal) {
    }

    ngOnInit() {
        this.times = [];
        this.rooms = [];
        this.loadAll();
    }

    showSection(section: any) {
        const modalRef = this.modalService.open(SectionComponent, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.section = section;
        modalRef.componentInstance.progress = this.diffMinsFromNow(section);
        modalRef.result.then(
            result => {
            },
            reason => {
            }
        );
    }

    loadAll() {
        const dateOfDay = new Date(this.dateOfDay);
        this.sectionService.query(this.conference.id, dateOfDay).subscribe(
            (res: HttpResponse<Section[]>) => this.onSuccess(res.body),
            (res: HttpResponse<any>) => this.onError(res.body)
        );
    }

    createSection(section: any) {
        const modalRef = this.modalService.open(SectionUpdateComponent, {size: 'lg', backdrop: 'static'});
        if (section != null) {
            modalRef.componentInstance.section = section;
        } else {
            const section1 = new Section();
            section1.conference = this.conference;
            section1.date = this.dateOfDay;
            modalRef.componentInstance.section = section1;
        }

        modalRef.result.then(
            result => {

            },
            reason => {
                if (reason.body) {
                    this.loadAll();
                }
            }
        );
    }

    private setRoomsWidth() {
        this.roomsWidth = ((100 - 8.333333) / this.rooms.length);
    }

    private onError(error) {
    }

    private onSuccess(data) {
        this.sections = data;
        if (this.sections.length > 0) {
            this.getRooms();
            this.sectionTimes(this.sections[0].timeFrom, this.sections[this.sections.length - 1].timeTo);
        } else {
            this.rooms = [];
            this.times = [];
        }
    }

    private getRooms() {
        for (let i = 0; i < this.sections.length; i++) {
            const room = this.sections[i].room;
            if (room != null) {
                if (this.rooms.indexOf(room) == -1) {
                    this.rooms.push(this.sections[i].room);
                }
            }
        }
        this.rooms.sort();
        this.setRoomsWidth();
    }

    pixelsFromTop(timeFrom: any) {
        const timeTokens = timeFrom.split(':');
        const firstTimeTokens = this.times[0].split(':');
        const date = new Date(1970, 0, 1, timeTokens[0], timeTokens[1], timeTokens[2]);
        const date2 = new Date(1970, 0, 1, firstTimeTokens[0], firstTimeTokens[1], 0);
        const difHMin = (date.getTime() - date2.getTime()) / 60000;
        return difHMin + 30;
    }

    minsBetween(timeFrom: any, timeTo: any) {
        const timeTokensFrom = timeFrom.split(':');
        const timeTokensTo = timeTo.split(':');
        const date = new Date(1970, 0, 1, timeTokensFrom[0], timeTokensFrom[1], timeTokensFrom[2]);
        const date2 = new Date(1970, 0, 1, timeTokensTo[0], timeTokensTo[1], timeTokensTo[2]);
        const diffMin = (date2.getTime() - date.getTime()) / 60000;
        return diffMin-1;
    }

    sectionTimes(timeFrom: any, timeTo: any) {
        this.times = [];
        const timeTokensFrom = timeFrom.split(':');
        const timeTokensTo = timeTo.split(':');
        const date = new Date(1970, 0, 1, timeTokensFrom[0], 0, 0);
        let date2;
        if (timeTokensTo[1] == '00' || timeTokensTo[2] == '00') {
            date2 = new Date(1970, 0, 1, timeTokensTo[0], 0, 0);
        } else {
            date2 = new Date(1970, 0, 1, (timeTokensTo[0] + 1), 0, 0);
        }
        const diffHours = (date2.getTime() - date.getTime()) / (60000 * 60);

        for (let i = 0; i <= diffHours; i++) {
            this.times.push(((date.getHours() + i) < 10 ? ('0' + (date.getHours() + i)) : (date.getHours() + i)) + ':00');
        }
    }

    diffMinsFromNow(section: any) {
        const sectionDate = new Date(section.date);
        const now = new Date();
        if (sectionDate.getDate() == now.getDate()) {
            const date = formatDate((new Date()), 'yyyy/MM/dd/HH/mm/ss', 'en');
            const dateTokensNow = date.split('/');
            const timeTokensFrom = section.timeFrom.split(':');
            const timeTokensTo = section.timeTo.split(':');

            const dateNow = new Date(+dateTokensNow[0], +dateTokensNow[1], +dateTokensNow[2]
                , +dateTokensNow[3], +dateTokensNow[4], +dateTokensNow[5]);
            const dateFrom = new Date(+dateTokensNow[0], +dateTokensNow[1], +dateTokensNow[2]
                , timeTokensFrom[0], timeTokensFrom[1], timeTokensFrom[2]);
            const dateTo = new Date(+dateTokensNow[0], +dateTokensNow[1], +dateTokensNow[2]
                , timeTokensTo[0], timeTokensTo[1], timeTokensTo[2]);
            if (dateNow.getTime() > dateFrom.getTime() && dateNow.getTime() < dateTo.getTime()) {
                const diffMinsFromTo = this.minsBetween(section.timeFrom, section.timeTo);
                const diffMinsNowFrom = (100 / diffMinsFromTo) * ((dateNow.getTime() - dateFrom.getTime()) / 60000);
                return diffMinsNowFrom;
            } else if (dateNow.getTime() > dateTo.getTime()) {
                return 100;
            } else {
                return 0;
            }
        }
    }

    deleteSection(section: Section) {
        const modalRef = this.modalService.open(SectionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.section = section;
        modalRef.result.then(
            result => {
            },
            reason => {
                if (reason != 'cancel') {
                    this.loadAll();
                }
            }
        );
    }

    private checkRooms(room: string) {
        let isUsed = false;
        for (let i = 0; i < this.sections.length; i++) {
            if (this.sections[0].room == room) {
                isUsed = true;
            }
        }

        if (!isUsed) {
            const index = this.rooms.indexOf(room);
            this.rooms.splice(index, 1);
            this.setRoomsWidth();
        }
    }

    getRoomIndex(room: string) {
        const index = this.rooms.indexOf(room);
        return index;
    }
}
