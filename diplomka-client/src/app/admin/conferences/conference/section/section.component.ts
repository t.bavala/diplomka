import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Article} from './article/article.model';
import {Section} from './section.model';
import {HttpResponse} from '@angular/common/http';
import {ArticleService} from './article/article.service';

@Component({
    selector: 'app-section',
    templateUrl: './section.component.html',
    styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit {
    section: Section;
    progress: any;
    articles: Article[];
    timeFrom: any;
    timeTo: any;

    constructor(public activeModal: NgbActiveModal, private articleService: ArticleService) {
        this.timeFrom = '';
        this.timeTo = '';
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    ngOnInit(): void {
        this.articleService.query(this.section.id).subscribe(
            (res: HttpResponse<Article[]>) => this.articles = res.body
        );

        this.sectionTimes(this.section.timeFrom, this.section.timeTo);
    }

    private sectionTimes(timeFrom: any, timeTo: any) {
        const timeFromTokens = timeFrom.split(':');
        const timeToTokens = timeTo.split(':');
        this.timeFrom = (timeFromTokens[0] + ':' + timeFromTokens[1]);
        this.timeTo = (timeToTokens[0] + ':' + timeToTokens[1]);
    }
}
