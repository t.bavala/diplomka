import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {Section} from "./section.model";

@Injectable({providedIn: 'root'})
export class SectionService {
    public resourceUrl = 'api/section';

    constructor(private http: HttpClient) {}

    query(id, date): Observable<HttpResponse<Section[]>> {
        const options = new HttpParams();
        options.append('date' , date);
        return this.http.get<Section[]>(`${'api/sections'}/${id}`, {params: {'date': date}, observe: 'response'});
    }

    create(section: Section): Observable<HttpResponse<Section>> {
        return this.http.post<Section>(this.resourceUrl, section, { observe: 'response' });
    }

    update(section: Section): Observable<HttpResponse<Section>> {
        return this.http.put<Section>(this.resourceUrl, section, { observe: 'response' });
    }

    delete(id: any): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    upload(sectionFiles: any, id): Observable<HttpResponse<any>> {
        return this.http.post(`${this.resourceUrl}/upload/${id}`, sectionFiles, { observe: 'response' });
    }
}
