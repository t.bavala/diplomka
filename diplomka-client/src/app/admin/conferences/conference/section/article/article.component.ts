import {Component, Input, OnInit} from '@angular/core';
import {Article} from './article.model';

@Component({
    selector: 'app-article',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
    @Input() article: Article;
    fileUrl: any;
    show: boolean;

    constructor() {
    }

    ngOnInit() {
        this.show = false;
        this.fileUrl = '../../../../../../uploads/conferences/' + this.article.section.conference.id + '/' + this.article.section.id + '/'
    }

    toggle() {
        this.show = !this.show;
    }
}
