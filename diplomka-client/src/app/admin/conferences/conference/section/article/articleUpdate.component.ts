import {Component, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Article} from "./article.model";
import {ArticleService} from "./article.service";

@Component({
    selector: 'articleUpdate',
    templateUrl: './articleUpdate.component.html'
})
export class ArticleUpdateComponent implements OnInit{
    article: Article;
    articleFile: any;
    isSaving: boolean;
    uploadDirectory: any;

    constructor(private articleService: ArticleService, public activeModal: NgbActiveModal) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save(article) {
        this.isSaving = true;
        this.articleService.update([article]).subscribe(response => {
            this.activeModal.dismiss(article);
        });
    }

    getFileDetails(e) {
        this.article.filename = e.target.files[0].name;
        this.articleFile.push(e.target.files[0]);
    }

    ngOnInit(): void {
        this.uploadDirectory = '../../../../../uploads/conferences/' + this.article.section.conference.id + '/' + this.article.section.id + '/';
        this.isSaving = false;
    }

    selectedUser(article) {
        this.article.user = article.user;
    }
}
