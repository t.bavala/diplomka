import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {ArticleService} from './article.service';
import {Article} from './article.model';

@Component({
    selector: 'articleDeleteDialog',
    templateUrl: './articleDeleteDialog.component.html'
})
export class ArticleDeleteDialogComponent {
    article: Article;

    constructor(private articleService: ArticleService, public activeModal: NgbActiveModal) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(article) {
        this.articleService.delete(article.id).subscribe(response => {
            this.activeModal.dismiss(article);
        });
    }
}
