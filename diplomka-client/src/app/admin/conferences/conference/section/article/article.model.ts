import {Section} from '../section.model';
import {User} from '../../../../../account/user.model';

export class Article {
    constructor(
        public id?: any,
        public title?: string,
        public filename?: string,
        public authors?: string,
        public abstractText?: string,
        public user?: User,
        public section?: Section
    ) {
        this.id = id ? id : null;
        this.title = title ? title : null;
        this.filename = filename ? filename : null;
        this.authors = authors ? authors : null;
        this.abstractText = abstractText ? abstractText : null;
        this.user = user ? user : null;
        this.section = section ? section : null;
    }
}
