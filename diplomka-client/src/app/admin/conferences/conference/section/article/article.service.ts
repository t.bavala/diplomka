import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Article} from './article.model';
import {User} from "../../../../../account/user.model";

@Injectable({providedIn: 'root'})
export class ArticleService {
    public resourceUrl = 'api/article';

    constructor(private http: HttpClient) {}

    query(id): Observable<HttpResponse<Article[]>> {
        const options = new HttpParams();
        return this.http.get<Article[]>(`${'api/articles'}/${id}`, {params: options, observe: 'response'});
    }

    update(articles: Article[]): Observable<HttpResponse<Article[]>> {
        return this.http.put<Article[]>('api/articles', articles, { observe: 'response' });
    }

    delete(id: any): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getAll(): Observable<HttpResponse<Article[]>> {
        return this.http.get<Article[]>('api/articlesConference', {observe: 'response'});
    }

    getUserArticles(id: any): Observable<HttpResponse<Article[]>> {
        return this.http.get<Article[]>(`${'api/articlesConference'}/${id}`, {observe: 'response'});
    }
}
