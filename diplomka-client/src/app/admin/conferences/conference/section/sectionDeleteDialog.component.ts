import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Section} from "./section.model";
import {SectionService} from "./section.service";

@Component({
    selector: 'sectionDeleteDialog',
    templateUrl: './sectionDeleteDialog.component.html'
})
export class SectionDeleteDialogComponent {
    section: Section;

    constructor(private sectionService: SectionService, public activeModal: NgbActiveModal) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(section) {
        this.sectionService.delete(section.id).subscribe(response => {
            this.activeModal.dismiss(section);
        });
    }
}
