import {Component, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SectionService} from './section.service';
import {Section} from './section.model';
import {Article} from './article/article.model';
import {HttpResponse} from '@angular/common/http';
import {ArticleService} from './article/article.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticleDeleteDialogComponent} from './article/articleDeleteDialog.component';

@Component({
    templateUrl: './sectionUpdate.component.html',
    styleUrls: ['./section.component.scss']
})
export class SectionUpdateComponent implements OnInit {
    section: Section;
    isSaving: boolean;
    articles: Article[];
    sectionFiles: any;
    uploadDirectory: any;


    constructor(private sectionService: SectionService,
                private articleService: ArticleService,
                public activeModal: NgbActiveModal,
                private route: ActivatedRoute,
                private router: Router,
                private modalService: NgbModal ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    ngOnInit(): void {
        this.sectionFiles = [];
        this.articles = [];
        if (this.section.id != null) {
            this.loadAll();
        }
        this.uploadDirectory = '../../../../../uploads/conferences/' + this.section.conference.id + '/' + this.section.id + '/';
        this.isSaving = false;
        // this.route.data.subscribe(({section}) => {
        //     this.section = section.body ? section.body : section;
        // });
    }

    save() {
        this.isSaving = true;
        if (this.section.id !== null) {
            this.sectionService.update(this.section).subscribe(response => this.onSaveSuccess(response), () => this.onSaveError());
        } else {
            this.sectionService.create(this.section).subscribe(response => this.onSaveSuccess(response), () => this.onSaveError());
        }

        this.articleService.update(this.articles).subscribe();

        const frmData = new FormData();

        for (let i = 0; i < this.sectionFiles.length; i++) {
            frmData.append('files', this.sectionFiles[i]);
        }

        if(this.sectionFiles.length > 0)
            this.sectionService.upload(frmData, this.section.id).subscribe();
        //
        // this.httpService.post('http://localhost:60505/api/fileupload/', frmData).subscribe(
        //     data => {
        //         // SHOW A MESSAGE RECEIVED FROM THE WEB API.
        //         this.sMsg = data as string;
        //         console.log(this.sMsg);
        //     },
        //     (err: HttpErrorResponse) => {
        //         console.log(err.message);    // Show error, if any.
        //     }
        // );
    }

    private onSaveSuccess(result) {
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    addArticle() {
        const article = new Article();
        article.section = this.section;
        this.articles.push(article);
    }

    private loadAll() {
        this.articleService.query(this.section.id).subscribe(
            (res: HttpResponse<Article[]>) => this.onSuccess(res.body),
            (res: HttpResponse<any>) => this.onError(res.body)
        );
    }

    private onSuccess(body: Article[]) {
        this.articles = body;
    }

    private onError(body: any) {

    }

    deleteArticle(article: Article) {
        if (article.id != null) {
            const modalRef = this.modalService.open(ArticleDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
            modalRef.componentInstance.article = article;
            modalRef.result.then(
                result => {
                },
                reason => {
                    if (reason !== 'cancel') {
                        this.articles.splice(this.articles.indexOf(article), 1);
                    }
                }
            );
        } else {
            this.articles.splice(this.articles.indexOf(article), 1);
        }
    }

    getFileDetails(e, article: Article) {
        const index = this.articles.indexOf(article);
        this.articles[index].filename = e.target.files[0].name;
        this.sectionFiles.push(e.target.files[0]);
    }

    selectedUser(article) {
        const index = this.articles.indexOf(article);
        this.articles[index].user = article.user;
    }
}
