import {Time} from '@angular/common';
import {Conference} from '../../conference.model';
import {Article} from './article/article.model';

export class Section {
    constructor(
        public id?: any,
        public name?: string,
        public title?: string,
        public assistants?: string,
        public date?: Date,
        public description?: string,
        public forAll?: boolean,
        public room?: string,
        public timeFrom?: Time,
        public timeTo?: Time,
        public conference?: Conference,
        public user?: any,
        public chair?: string,
        public articles?: Article
    ) {
        this.id = id ? id : null;
        this.name = name ? name : null;
        this.title = title ? title : null;
        this.assistants = assistants ? assistants : null;
        this.date = date ? date : null;
        this.description = description ? description : null;
        this.forAll = forAll ? forAll : false;
        this.room = room ? room : null;
        this.timeFrom = timeFrom ? timeFrom : null;
        this.timeTo = timeTo ? timeTo : null;
        this.conference = conference ? conference : null;
        this.user = user ? user : null;
        this.chair = chair ? chair : null;
        this.articles = articles ? articles : null;
    }
}
