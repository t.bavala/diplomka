import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Conference} from '../conference.model';
import {ConferencesService} from '../conferences.services';
import {UserService} from '../../userManagement/user.service';
import {User} from '../../../account/user.model';
import {HttpResponse} from '@angular/common/http';
import {NgbDateAdapter, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';

@Component({
    selector: 'conferenceUpdate',
    templateUrl: './conferenceUpdate.component.html',
    styleUrls: ['./conferenceUpdate.component.scss']
})
export class ConferenceUpdateComponent implements OnInit, OnDestroy {
    conference: Conference;
    isSaving: boolean;
    users: User[];
    markDisabled;
    private searchUsers: User[];
    searchInput: any;
    private searchShow: boolean;
    conferenceUsers: User[];

    constructor(
        private conferencesService: ConferencesService,
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private ngbDateAdapter: NgbDateAdapter<Date>,
        private ngbDateStructAdapter: NgbDateAdapter<NgbDateStruct>
    ) {
    }

    ngOnInit() {
        const element = document.getElementById('header');
        element.classList.add('fixed-navbar');
        this.markDisabled = (date: NgbDate) => {
            return [new Date(2019, 4, 18)];
        };
        this.isSaving = false;
        this.route.data.subscribe(({conference}) => {
            this.conference = conference.body ? conference.body : conference;
        });
        this.users = [];
        this.searchUsers = [];
        this.conferenceUsers = [];
        if (this.conference.users)
            this.conferenceUsers = this.conference.users;
        this.userService.query().subscribe(
            (res: HttpResponse<User[]>) => this.onSuccess(res.body),
            (res: HttpResponse<any>) => this.onError(res.body)
        );
    }

    private onSuccess(body: User[]) {
        const allUsers = body;
        this.users = [];
        this.searchUsers = [];
        if (this.conference.users) {
            for (let i = 0; i < allUsers.length; i++) {
                if (this.conference.users.indexOf(allUsers[i]) == -1){
                    this.users.push(allUsers[i]);
                    this.searchUsers.push(allUsers[i]);
                }
            }
        } else {
            this.users = allUsers;
            this.searchUsers = allUsers;
        }
    }


    private onError(error) {
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        const dateFrom = this.ngbDateAdapter.fromModel(this.conference.dateFrom);
        const dateTo = this.ngbDateAdapter.fromModel(this.conference.dateTo);
        const fromDate = new Date(dateFrom.year, dateFrom.month - 1, dateFrom.day);
        const toDate = new Date(dateTo.year, dateTo.month - 1, dateTo.day);
        this.conference.dateFrom = fromDate;
        this.conference.dateTo = toDate;
        if (this.conference.id !== null) {
            this.conferencesService.update(this.conference).subscribe(response => this.onSaveSuccess(response), () => this.onSaveError());
        } else {
            this.conferencesService.create(this.conference).subscribe(response => this.onSaveSuccess(response), () => this.onSaveError());
        }
    }

    private onSaveSuccess(result) {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    ngOnDestroy(): void {
        const element = document.getElementById('header');
        element.classList.remove('fixed-navbar');
    }

    myFunction() {
        const users = [];
        if (this.searchInput === '') {
            this.searchUsers = this.users;
        } else {
            for (let i = 0; i < this.users.length; i++) {
                if ((this.users[i].username).indexOf(this.searchInput) > -1) {
                    users.push(this.users[i]);
                }
            }
            this.searchUsers = users;
        }
    }

    searchUsersShow() {
        this.searchShow = true;
    }

    searchUsersHide() {
        this.searchShow = false;
    }

    selectUser(user: User) {
        if (this.conference.users){
            if (this.conference.users.indexOf(user) == -1) {
                this.conferenceUsers.push(user);
                this.conference.users = this.conferenceUsers;
            }
        } else {
            this.conferenceUsers.push(user);
            this.conference.users = this.conferenceUsers;
        }

        this.searchShow = false;
    }

    deleteUser(conferenceUser: User) {
        const index = this.conferenceUsers.indexOf(conferenceUser)
        this.conferenceUsers.splice(index,1);
        this.conference.users = this.conferenceUsers;
    }
}
