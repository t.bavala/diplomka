import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import {RouterModule} from "@angular/router";
import {ConferencesComponent} from "./conferences.component";
import {ConferenceUpdateComponent} from "./conference/conferenceUpdate.component";
import {conferencesRoute} from "./conferences.route";
import {SharedModule} from "../../shared/shared.module";
import {ConferenceComponent} from "./conference/conference.component";
import { ConferenceDayComponent } from './conference/conference-day/conference-day.component';
import { SectionComponent } from './conference/section/section.component';
import {SectionUpdateComponent} from "./conference/section/sectionUpdate.component";
import {SectionDeleteDialogComponent} from "./conference/section/sectionDeleteDialog.component";
import {ConferenceDeleteDialogComponent} from "./conference/conferenceDeleteDialog.component";
import { ArticleComponent } from './conference/section/article/article.component';
import {ArticleDeleteDialogComponent} from "./conference/section/article/articleDeleteDialog.component";

@NgModule({
    imports: [
        RouterModule.forChild(conferencesRoute),
        SharedModule
    ],
    declarations: [
        ConferencesComponent,
        ConferenceUpdateComponent,
        ConferenceComponent,
        ConferenceDayComponent,
        SectionComponent,
        SectionUpdateComponent,
        SectionDeleteDialogComponent,
        ConferenceDeleteDialogComponent,
        ArticleComponent,
        ArticleDeleteDialogComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    entryComponents: [
        SectionComponent,
        SectionUpdateComponent,
        SectionDeleteDialogComponent,
        ConferenceDeleteDialogComponent,
        ArticleDeleteDialogComponent
    ],
    exports: [ConferenceDayComponent, ArticleComponent]
})
export class ConferencesModule {
    constructor() {
    }
}
