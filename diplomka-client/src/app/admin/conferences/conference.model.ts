import {User} from "../../account/user.model";
import {Address} from "../../account/address.model";

export class Conference {
    constructor(
        public id?: any,
        public title?: string,
        public dateFrom?: Date,
        public dateTo?: Date,
        public description?: string,
        public users?: User[],
        public address?: Address
    ) {
        this.id = id ? id : null;
        this.title = title ? title : null;
        this.dateFrom = dateFrom ? dateFrom : null;
        this.dateTo = dateTo ? dateTo : null;
        this.description = description ? description : null;
        this.users = users ? users : null;
        this.address = address ? address : new Address();
    }
}
