import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, Routes} from '@angular/router';
import {userManagementRoute} from "./userManagement/userManagement.route";
import {Injectable} from "@angular/core";
import {AccountService} from "../account/account.service";

const ADMIN_ROUTES = [...userManagementRoute];

@Injectable({ providedIn: 'root' })
export class UserResolve implements CanActivate {
    constructor(private accountService: AccountService, private router: Router) {}

    canActivate(route : ActivatedRouteSnapshot , state : RouterStateSnapshot): Promise<boolean> {
        return this.accountService.identity().then(
            account => {
                if(this.accountService.hasAnyRole(['SUPERADMIN'])){
                    return true;
                } else {
                    this.router.navigate(['/']);
                    return false;
                }
            }
        );
    }
}

export const adminState: Routes = [
    {
        path: '',
        data: {
            authorities: ['SUPERADMIN']
        },
        canActivate: [UserResolve],
        children: ADMIN_ROUTES
    }
];
