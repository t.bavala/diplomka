export * from './userManagement/userManagement.component';
export * from './userManagement/userMgmtDeleteDialog.component';
export * from './userManagement/userManagement.route';
export * from './admin.route';
export * from './userManagement/userMgmtUpdate.component';
export * from './userManagement/userMgmtDetail.component';
