import { Component, OnInit, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { LoginService } from "../login/login.service";
import { Register } from './register.service';
import {User} from "../account/user.model";

@Component({
  selector: 'register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit, AfterViewInit {
  confirmPassword: string;
  doNotMatch: string;
  error: string;
  errorEmailExists: string;
  errorUserExists: string;
  registerAccount: any;
  success: boolean;

  constructor(
    private loginService: LoginService,
    private registerService: Register,
    private elementRef: ElementRef,
    private renderer: Renderer
  ) {}

  ngOnInit() {
    this.success = false;
    this.registerAccount = new User;
  }

  ngAfterViewInit() {
    this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#username'), 'focus', []);
  }

  register() {
    if (this.registerAccount.password !== this.confirmPassword) {
      this.doNotMatch = 'ERROR';
    } else {
      this.doNotMatch = null;
      this.error = null;
      this.errorUserExists = null;
      this.errorEmailExists = null;

      this.registerService.save(this.registerAccount).subscribe(
        () => {
          this.success = true;
        },
        response => this.processError(response)
      );
    }
  }

  private processError(response: HttpErrorResponse) {
    // this.success = null;
    // if (response.status === 400 && response.error.type === LOGIN_ALREADY_USED_TYPE) {
    //   this.errorUserExists = 'ERROR';
    // } else if (response.status === 400 && response.error.type === EMAIL_ALREADY_USED_TYPE) {
    //   this.errorEmailExists = 'ERROR';
    // } else {
    //   this.error = 'ERROR';
    // }
  }
}
