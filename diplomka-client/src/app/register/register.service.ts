import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class Register {
  constructor(private http: HttpClient) {}

  save(user: any): Observable<any> {
    return this.http.post('/api/register', user);
  }
}
